/**
 * @file
 * Js file for add/edit class.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.teacherPayment = {
    attach: function (context) {

      var number = $('.field--type-teacher-lesson-payment .static-amount', context);
      var amount = $('.field--type-teacher-lesson-payment .student-amount', context);

      $('.field--type-teacher-lesson-payment .student-amount .hide-on-load').hide();

      // Function to show/hide fields.
      var showFields = function(value) {
        switch (value) {
          case 'none':
          case 'manual':
            number.hide();
            amount.hide();
            break;
          case 'static':
            number.show();
            amount.hide();
            break;
          case 'students_depends':
            amount.show();
            number.hide();
            break;
        }
      };
      // On load action.
      var value = $('.field--type-teacher-lesson-payment .form-type-select select', context).val();
      if (typeof value === "undefined") {
        number.hide();
        amount.hide();
      } else {
        showFields(value);
      }

      // On change action.
      $('.field--type-teacher-lesson-payment .form-type-select select', context).on('change', function() {
        var value = $(this).val();
        showFields(value);
      });

      // Add fields.
      var last = $('.field--type-teacher-lesson-payment .student-amount .last', context);
      $('.field--type-teacher-lesson-payment .student-amount .plus', context).on('click', function() {
        // Count.
        var numItems = $(document).find('.student-amount .to-count').length;
        var cloned = $('.field--type-teacher-lesson-payment .student-amount .to-clone', context).clone().removeClass('hidden').removeClass('to-clone').addClass('to-count');
        var name = cloned.find('input').prop('name').replace('hidden', numItems+1);
        cloned.find('input').prop('name', name);
        var prefix = cloned.find('.prefix').text().replace('[NUMBER]', numItems+1);
        cloned.find('.prefix').text(prefix);
        // Hide prev minuses.
        $(document).find('.student-amount .to-count .minus').hide();
        last.before(cloned);
      });
      // Remove field.
      $(document).on("click", ".field--type-teacher-lesson-payment .student-amount .minus" , function() {
        $(this).parent().remove();
        $(document).find('.student-amount .to-count .minus').last().show();
      });
    }
  };

})(jQuery, Drupal, drupalSettings);