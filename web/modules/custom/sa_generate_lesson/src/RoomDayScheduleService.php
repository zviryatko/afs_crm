<?php

/**
 * @file
 * Contains \Drupal\sa_generate_lesson\RoomDayScheduleService.
 */

namespace Drupal\sa_generate_lesson;

use Drupal\node\NodeInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\node\Entity\Node;
use Drupal\Core\Datetime\DrupalDateTime;

class RoomDayScheduleService {

  protected $entityQuery;

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.query')
    );
  }

  public function __construct(QueryFactory $entityQuery) {
    $this->entityQuery = $entityQuery;
  }

  /**
   * Return lessons for the given date.
   *
   * @param NodeInterface $class
   * @param $year
   * @param $month
   * @param $day
   * @return array|\Drupal\Core\Entity\EntityInterface[]|static[]
   */
  public function getLessonsListForADay(NodeInterface $class, $year, $month, $day) {

    $room = $class->get('field_room')->target_id;
    $branch = $class->get('field_branch')->target_id;
    $bundle = $class->bundle();

    if (!empty($room) && !empty($branch) && !empty($bundle)) {
      $query = $this->entityQuery->get('node');
      $query->condition('status', 1);
      $query->condition('type', $bundle);
      $query->condition('field_branch', $branch, '=');
      $query->condition('field_room', $room, '=');
      $classes_ids = $query->execute();

      $time = new \DateTime;
      $lesson_from = $time->createFromFormat('Y-m-d H:i', $year . '-' . $month . '-' . $day . ' 00:00');
      $timestamp_from = $lesson_from->format('U');
      $lesson_to = $time->createFromFormat('Y-m-d H:i', $year . '-' . $month . '-' . ($day + 1) . ' 00:00');
      $timestamp_to = $lesson_to->format('U');

      $query = $this->entityQuery->get('node');
      $query->condition('status', 1);
      $query->condition('type', 'lesson');
      $query->condition('field_class', $classes_ids, 'IN');
      $query->condition('field_lesson_from_time', $timestamp_from, ">");
      $query->condition('field_lesson_to_time', $timestamp_to, "<");
      $lessons_ids = $query->execute();
      $lessons = Node::loadMultiple($lessons_ids);
      return $lessons;
    }
    return [];
  }

  /**
   * Check if time is free for given class.
   *
   * @param NodeInterface $class
   * @param DrupalDateTime $date
   * @param $time_from
   * @param $time_to
   * @param $nid
   *  Curent node nid to unset.
   * @return bool
   */
  public function checkIfTimeIsFree(NodeInterface $class, DrupalDateTime $date, $time_from, $time_to, $nid = NULL) {

    $room = $class->get('field_room')->target_id;
    $branch = $class->get('field_branch')->target_id;
    $bundle = $class->bundle();

    if (!empty($room) && !empty($branch) && !empty($bundle)) {
      $query = $this->entityQuery->get('node');
      $query->condition('status', 1);
      $query->condition('type', $bundle);
      $query->condition('field_branch', $branch, '=');
      $query->condition('field_room', $room, '=');
      $classes_ids = $query->execute();

      $time = new \DateTime;
      $lesson_from = $time->createFromFormat('Y-m-d H:i', $date->format('Y-m-d') . ' ' . $time_from);
      $timestamp_from = $lesson_from->format('U') + 60;
      $lesson_to = $time->createFromFormat('Y-m-d H:i', $date->format('Y-m-d') . ' ' . $time_to);
      $timestamp_to = $lesson_to->format('U') - 60;

      if ($timestamp_to <= $timestamp_from) {
        return FALSE;
      }

      $query = $this->entityQuery->get('node');
      $group_and_from = $query->andConditionGroup()
        ->condition('field_lesson_from_time', $timestamp_from, "<=")
        ->condition('field_lesson_to_time', $timestamp_from, ">=");
      $group_and_to = $query->andConditionGroup()
        ->condition('field_lesson_from_time', $timestamp_to, "<=")
        ->condition('field_lesson_to_time', $timestamp_to, ">=");
      $group_or = $query->orConditionGroup()
        ->condition($group_and_from)
        ->condition($group_and_to);
      $query->condition('status', 1);
      $query->condition('type', 'lesson');
      $query->condition('field_class', $classes_ids, 'IN');
      $query->condition($group_or);

      $lessons_ids = $query->execute();

      if ($nid && $nid_to_delete = array_search($nid, $lessons_ids)) {
        unset($lessons_ids[$nid_to_delete]);
      }

      if (empty($lessons_ids)) {
        return TRUE;
      }

      return FALSE;
    }
    return FALSE;
  }

  /**
   * Check If Time Is Free For a Lesson.
   *
   * @param $date
   * @param $time_from
   * @param $time_to
   * @param $room
   * @param $branch
   * @return bool
   */
  public function checkIfTimeIsFreeForLesson($date, $time_from, $time_to, $room, $branch) {

    $bundle = 'classes';

    if (!empty($room) && !empty($branch) && !empty($bundle)) {
      $query = $this->entityQuery->get('node');
      $query->condition('status', 1);
      $query->condition('type', $bundle);
      $query->condition('field_branch', $branch, '=');
      $query->condition('field_room', $room, '=');
      $classes_ids = $query->execute();

      if (empty($classes_ids)) {
        return TRUE;
      }

      $time = new \DateTime;
      $lesson_from = $time->createFromFormat('Y-m-d H:i', $date . ' ' . $time_from);
      $timestamp_from = $lesson_from->format('U') + 60;
      $lesson_to = $time->createFromFormat('Y-m-d H:i', $date . ' ' . $time_to);
      $timestamp_to = $lesson_to->format('U') - 60;

      if ($timestamp_to <= $timestamp_from) {
        return FALSE;
      }

      $query = $this->entityQuery->get('node');
      $group_and_from = $query->andConditionGroup()
        ->condition('field_lesson_from_time', $timestamp_from, "<=")
        ->condition('field_lesson_to_time', $timestamp_from, ">=");
      $group_and_to = $query->andConditionGroup()
        ->condition('field_lesson_from_time', $timestamp_to, "<=")
        ->condition('field_lesson_to_time', $timestamp_to, ">=");
      $group_or = $query->orConditionGroup()
        ->condition($group_and_from)
        ->condition($group_and_to);
      $query->condition('status', 1);
      $query->condition('type', 'lesson');
      $query->condition('field_class', $classes_ids, 'IN');
      $query->condition($group_or);

      $lessons_ids = $query->execute();

      if (empty($lessons_ids)) {
        return TRUE;
      }

      return FALSE;
    }
    return FALSE;
  }

}