<?php

namespace Drupal\sa_generate_lesson\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'teacher_lesson_payment' formatter.
 *
 * @FieldFormatter(
 *   id = "teacher_lesson_payment_formatter",
 *   label = @Translation("Teacher Payment"),
 *   field_types = {
 *     "teacher_lesson_payment"
 *   }
 * )
 */
class TeacherPaymentFormatter extends FormatterBase
{

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $element = [];
    $values = [];
    foreach ($items as $item) {
      $values = $item->getValue();
    }
    switch ($values['type']) {
      case 'static';
        $value = t('Static value per lesson') . ' - ' . $values['amount'] . '.';
        break;
      case 'none':
        $value = t('Teacher payed peer month.');
        break;
      case 'manual':
        $value =  t('Manually set per lesson.');
        break;
      default:
        $value = t('Depends on a student amount.');
        $defs = json_decode($values['definitions']);
        foreach ($defs as $key => $def) {
          $value .=  $key . ' - ' . $def . ', ';
        }
        $value .= '.';
    }

    $element['#markup'] = '<div class="field field--name-teacher-payment field--label-above">
    <div class="field__label">' . t('Teacher payment type for a lesson') . '</div><div>' . $value . '</div>
              </div>';

    return $element;
  }

}
