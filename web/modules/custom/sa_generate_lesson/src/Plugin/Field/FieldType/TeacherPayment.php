<?php

namespace Drupal\sa_generate_lesson\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'teacher_lesson_payment' field type.
 *
 * @FieldType(
 *   id = "teacher_lesson_payment",
 *   label = @Translation("Teacher payment for a lesson"),
 *   module = "sa_generate_lesson",
 *   description = @Translation("Describe how teacher will be payed for a lesson."),
 *   default_widget = "teacher_lesson_payment_widget",
 *   default_formatter = "teacher_lesson_payment_formatter"
 * )
 */
class TeacherPayment extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [];

    $columns['formula'] = [
      'description' => t('Formula'),
      'type' => 'text',
    ];
    $columns['definitions'] = [
      'description' => t('Definitions'),
      'type' => 'text',
    ];
    $columns['amount'] = [
      'description' => t('Amount'),
      'type' => 'int',
      'default' => 0,
    ];
    $columns['type'] = [
      'description' => t('Type'),
      'type' => 'varchar',
      'length' => 100,
      'not null' => TRUE,
      'default' => '',
    ];
    return ['columns' => $columns];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];

    $properties['definitions'] = DataDefinition::create('string')
        ->setLabel(t('Amount of visited students -> price'));
    $properties['amount'] = DataDefinition::create('integer')
        ->setLabel(t('Amount'));
    $properties['type'] = DataDefinition::create('string')
      ->setLabel(t('Type'));
    $properties['formula'] = DataDefinition::create('string')
      ->setLabel(t('Formula'));

    return $properties;
  }

}
