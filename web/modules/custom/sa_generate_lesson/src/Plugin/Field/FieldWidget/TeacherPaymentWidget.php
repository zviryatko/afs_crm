<?php

namespace Drupal\sa_generate_lesson\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\WidgetBase;

/**
 * Plugin implementation of the 'teacher_lesson_payment' widget.
 *
 * @FieldWidget(
 *   id = "teacher_lesson_payment_widget",
 *   module = "sa_generate_lessons",
 *   label = @Translation("Teacher payment type"),
 *   field_types = {
 *     "teacher_lesson_payment"
 *   }
 * )
 */
class TeacherPaymentWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $elements = [];
    $values = $items->getValue()[$delta];


    $elements['type'] = [
      '#type' => 'select',
      '#options' => [
        'manual' => t('Manually set per lesson'),
        'none' => t('Teacher payed peer month'),
        'static' => t('Static value per lesson'),
        'students_depends' => t('Depends on a student amount'),
      ],
      '#default_value' => $values['type'],
    ];

    $elements['amount'] = [
      '#type' => 'number',
      '#min' => 0,
      '#max' => 999999,
      '#size' => 5,
      '#default_value' => empty($values['amount']) ? 0 : $values['amount'],
      '#wrapper_attributes' => [
        'class' => ['static-amount'],
      ],
    ];

    $elements['student_amount'] = [
      '#type' => 'item',
      '#wrapper_attributes' => [
        'class' => ['student-amount'],
      ],
    ];

    // Decode definition.
    if ($values['type'] == 'students_depends') {
      $definitions = json_decode($values['definitions']);
      $max = 0;

      foreach ($definitions as $key => $item) {
        if ($key == 'any') {
          $elements['student_amount']['any'] = [
            '#type' => 'number',
            '#min' => 1,
            '#max' => 99999,
            '#size' => 5,
            '#prefix' => '<div class="item last"><span class="prefix inline-block">' . t('For ANY other student amount') . '</span>',
            '#suffix' => '</div>',
            '#wrapper_attributes' => [
              'class' => ['inline-block'],
            ],
            '#default_value' => $item,
          ];
        }
        elseif($key == 1) {
          $elements['student_amount'][$key] = [
            '#type' => 'number',
            '#min' => 1,
            '#max' => 99999,
            '#size' => 5,
            '#prefix' => '<div class="item with-number to-count"><span class="prefix inline-block">' . t('For @number student', ['@number' => $key]) . '</span>',
            '#suffix' => '<span class="suffix inline-block plus">+</span></div>',
            '#wrapper_attributes' => [
              'class' => ['inline-block'],
            ],
            '#default_value' => $item,
          ];
        }
        else {
          $max++;
          $elements['student_amount'][$key] = [
            '#type' => 'number',
            '#min' => 1,
            '#max' => 99999,
            '#size' => 5,
            '#prefix' => '<div class="item with-number to-count"><span class="prefix inline-block">' . t('For @number student', ['@number' => $key]) . '</span>',
            '#suffix' => '<span class="suffix inline-block minus hide-on-load">-</span></div>',
            '#wrapper_attributes' => [
              'class' => ['inline-block'],
            ],
            '#default_value' => $item,
          ];
        }
      }
      // Set minus.
      if ($max > 0) {
        $max++;
        $elements['student_amount'][$max]['#suffix'] = '<span class="suffix inline-block minus">-</span></div>';
      }

    } else {
      $elements['student_amount']['1'] = [
        '#type' => 'number',
        '#min' => 1,
        '#max' => 99999,
        '#size' => 5,
        '#prefix' => '<div class="item with-number to-count"><span class="prefix inline-block">' . t('For 1 student') . '</span>',
        '#suffix' => '<span class="suffix inline-block plus">+</span></div>',
        '#wrapper_attributes' => [
          'class' => ['inline-block'],
        ],
      ];
      $elements['student_amount']['any'] = [
        '#type' => 'number',
        '#min' => 1,
        '#max' => 99999,
        '#size' => 5,
        '#prefix' => '<div class="item last"><span class="prefix inline-block">' . t('For ANY other student amount') . '</span>',
        '#suffix' => '</div>',
        '#wrapper_attributes' => [
          'class' => ['inline-block'],
        ],
      ];
    }

    $elements['student_amount']['hidden'] = [
      '#type' => 'number',
      '#min' => 1,
      '#max' => 99999,
      '#size' => 5,
      '#prefix' => '<div class="hidden item with-number inline-block to-clone"><span class="prefix inline-block">' . t('For @number students', ['@number' => '[NUMBER]']) . '</span>',
      '#suffix' => '<span class="suffix inline-block minus">-</span></div>',
      '#wrapper_attributes' => [
        'class' => ['inline-block'],
      ],
    ];

    $elements['definitions'] = [
      '#type' => 'textarea',
      '#default_value' => $values['definitions'],
      '#access' => FALSE,
    ];

    // Wrap the whole form in a container.
    $elements += [
      '#type' => 'item',
      '#title' => $element['#title'],
      '#attached' => ['library' => ['sa_generate_lesson/teacher_payment']],
    ];

    return $elements;
  }

}
