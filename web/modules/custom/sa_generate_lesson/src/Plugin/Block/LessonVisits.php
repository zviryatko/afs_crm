<?php

namespace Drupal\sa_generate_lesson\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\NodeInterface;
use Drupal\node\Entity\Node;

/**
 * Provides a 'Lesson Visits' Block.
 *
 * @Block(
 *   id = "lesson_visits_block",
 *   admin_label = @Translation("Lesson Visits"),
 * )
 */
class LessonVisits extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $lesson = \Drupal::routeMatch()->getParameters()->get('node');

    if (!($lesson instanceof NodeInterface)) {
      return ['#markup' => ''];
    }
    $class = Node::load($lesson->get('field_class')->target_id);

    $class_students = [];
    foreach ($class->get('field_students') as $student) {
      $row = $this->checkStudent($student, $lesson, $class);

      if (!empty($row)) {
        $class_students = array_merge($class_students, $row);
      }
    }

    $additional_students = [];
    foreach ($lesson->get('field_students') as $student) {
      $row = $this->checkStudent($student, $lesson, $class);
      if (!empty($row)) {
        $additional_students = array_merge($additional_students, $row);
      }
    }

    // Add full name.
    foreach ($class_students as &$class_student) {
      if(!isset($class_student['user_id'])) {
        continue;
      }
      $class_student['user_name'] = get_user_full_name($class_student['user_id']);
    }
    foreach ($additional_students as &$additional_student) {
      if(!isset($additional_student['user_id'])) {
        continue;
      }
      $additional_student['user_name'] = get_user_full_name($additional_student['user_id']);
    }

    return [
      '#theme' => 'lesson_visits',
      '#class_students' => $class_students,
      '#additional_students' => $additional_students,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Generate Row.
   *
   * @param int $id
   *   Visit ID.
   * @param int $lesson
   *   Lesson ID.
   * @param string $status
   *   Status string.
   * @param string $user_id
   *   User name string.
   *
   * @return array
   */
  public function generateRow($id, $lesson, $status, $user_id) {
    $links = [];
    $checked = FALSE;
    foreach(visit_statuses() as $key => $title) {
      $class = 'use-ajax link ' . $key . '-' . $id;
      if ($key == $status) {
        $checked = TRUE;
        $class .= ' active';
      }
      $links[] = ['text' => $title, 'class' => $class, 'url' => '/sa-visits/lesson-callback/' . $id . '/' . $lesson . '/' . $key];
    }
    $class = $checked ? 'link use-ajax none-' . $id : 'link active use-ajax none-' . $id;
    // Add None.
    $links[] = ['text' => t('None'), 'class' => $class, 'url' => '/sa-visits/lesson-callback/' . $id . '/' . $lesson . '/none'];

    return [
      'user_id' => $user_id,
      'links' => $links,
      'row_class' => 'links row-' . $id,
    ];
  }

  public function loadProfitsByUser($user_id) {
    $now = new \DateTime();
    $now_t = $now->getTimestamp();
    $query = \Drupal::database()->select('profits_entity', 'p_e');
    $query->fields('p_e', ['id']);
    $query->fields('p_e_a', ['field_abonnement_target_id']);
    $query->leftJoin('profits_entity__field_user', 'p_e_u', 'p_e_u.entity_id=p_e.id');
    $query->leftJoin('profits_entity__field_abonnement', 'p_e_a', 'p_e_a.entity_id=p_e.id');
    $query->condition('p_e_a.field_abonnement_target_id', NULL, 'IS NOT NULL');
    $query->condition('p_e.to_timestamp', $now_t, '>=');
    $query->condition('p_e_u.field_user_target_id', $user_id, '=');
    $result = $query->execute();
    return $result->fetchAll();
  }

  public function loadVisitsByLesson($lesson_id, $payment_id) {
    $query = \Drupal::database()->select('visits_entity', 'v_e');
    $query->fields('v_e', ['id', 'status']);
    $query->condition('v_e.lesson', $lesson_id, '=');
    $query->condition('v_e.status', ['visited', 'missed', 'postponed'], 'IN');
    $query->condition('v_e.payment', $payment_id, '=');
    $query->range(0, 1);
    $result = $query->execute();
    $visits = $result->fetchAll();
    return reset($visits);
  }

  public function loadVisitsByPayment($payment_id) {
    $query = \Drupal::database()->select('visits_entity', 'v_e');
    $query->fields('v_e', ['id', 'component']);
    $query->condition('v_e.payment', $payment_id, '=');
    $query->condition('v_e.lesson', NULL, 'IS NULL');
    $query->orderBy('id', 'DESC');
    $result = $query->execute();
    $visits = $result->fetchAll();
    return $visits;
  }

  public function checkStudent($student, $lesson, $class) {
    $class_students = [];
    $payments = $this->loadProfitsByUser($student->target_id);
    foreach ($payments as $payment) {
      // Load visits by this lesson limit to one.
      $visit = $this->loadVisitsByLesson($lesson->id(), $payment->id);
      if (!empty($visit)) {
        $class_students[] = $this->generateRow($visit->id, $lesson->id(), $visit->status, $student->target_id);
        continue;
      }
      // Load visits with lessons empty by this payment.
      $visits = $this->loadVisitsByPayment($payment->id);
      $original_components = [];
      foreach ($visits as $visit) {
        $original_components[$visit->component] = $visit->id;
      }
      // Check every component if match current lesson.
      foreach ($original_components as $component => $visit_id) {
        $block = \Drupal::entityTypeManager()->getStorage('block_content')->load($component);
        if (empty($block)) {
          continue;
        }
        $limit = $block->get('field_lesson_limit')->value;
        switch ($limit) {
          case 'by_class':
            $block_classes = $block->get('field_classes');
            $block_classes_array = [];
            foreach ($block_classes as $block_class) {
              $block_classes_array[] = $block_class->target_id;
            }
            // Check if current lesson match.
            if (in_array($class->id(), $block_classes_array)) {
              $class_students[] = $this->generateRow($visit_id, $lesson->id(), '', $student->target_id);
            }
            break;

          case 'by_category':
            $block_categories = $block->get('field_categories');
            $block_categories_array = [];
            foreach ($block_categories as $block_category) {
              $block_categories_array[] = $block_category->target_id;
            }
            $class_categories = $class->get('field_category');
            $class_categories_array = [];
            foreach ($class_categories as $class_category) {
              $class_categories_array[] = $class_category->target_id;
            }
            $common_categories = array_intersect($block_categories_array, $class_categories_array);
            // Check if current class category match.
            if (!empty($common_categories)) {
              $class_students[] = $this->generateRow($visit->id, $lesson->id(), '', $student->target_id);
            }
            break;

          default:
            // No limits.
            $class_students[] = $this->generateRow($visit->id, $lesson->id(), '', $student->target_id);
        }
      }
    }

    return $class_students;
  }
}
