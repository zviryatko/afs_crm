<?php
namespace Drupal\sa_generate_lesson\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\NodeInterface;

/**
 * Provides route responses for the Example module.
 */
class LessonsForCalendarAjaxController extends ControllerBase {

  public function load(NodeInterface $lesson) {
    $entity_type = 'node';
    $view_mode = 'teaser';
    $view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity_type);
    return $view_builder->view($lesson, $view_mode);
  }

}