<?php
namespace Drupal\sa_generate_lesson\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\NodeInterface;
use Drupal\sa_generate_lesson\RoomDayScheduleService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Provides route responses for the Example module.
 */
class GetRoomDayScheduleController extends ControllerBase {

  protected $room_day_schedule;

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('room_day.schedule_service')
    );
  }

  public function __construct(RoomDayScheduleService $room_day_schedule) {
    $this->room_day_schedule = $room_day_schedule;
  }

  /**
   * @param NodeInterface $class
   * @param $year
   * @param $month
   * @param $day
   * @return JsonResponse
   */
  public function content(NodeInterface $class, $year, $month, $day) {
    $response = new JsonResponse();
    $lessons = $this->room_day_schedule->getLessonsListForADay($class, $year, $month, $day);
    $lessons_time_intervals = [];
    foreach ($lessons as $key => $lesson) {
      $from = $lesson->get('field_lesson_from_time')->value;
      $to = $lesson->get('field_lesson_to_time')->value;
      if (!empty($from) && !empty($to)) {
        $lessons_time_intervals[$key] = ['from' => $from, 'to' => $to];
      }
    }
    $start_time  = new \DateTime($year . '-' . $month . '-' . $day . ' 00:00');
    $end_time    = new \DateTime($year . '-' . $month . '-' . $day . ' 23:55');
    $time_step   = 5;
    $times_array  = [];
    $keep_time = 6;
    $i = 0;
    while($start_time <= $end_time) {
        if ((count($times_array) % $keep_time) == 0) {
          $times_array[$i]['label'] = $start_time->format('H:i');
        }
        else {
          $times_array[$i]['label'] = '';
        }
        $step_timestamp = $start_time->format('U');
        $times_array[$i]['class'] = 'one-time-slot';
        $times_array[$i]['lesson_id'] = 0;
        foreach ($lessons_time_intervals as $key => $lessons_time_interval) {
          if (($step_timestamp >= $lessons_time_interval['from']) && ($step_timestamp <= $lessons_time_interval['to'])) {
            $times_array[$i]['class'] .= ' busy';
            $times_array[$i]['lesson_id'] = $key;
          }
        }
        $start_time->add(new \DateInterval('PT' . $time_step . 'M'));
        $i++;
    }
    $response->setData($times_array);
    return $response;
  }

}