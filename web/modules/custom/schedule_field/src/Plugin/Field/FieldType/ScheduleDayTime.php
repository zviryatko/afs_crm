<?php

namespace Drupal\schedule_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'schedule_day_time' field type.
 *
 * @FieldType(
 *   id = "schedule_day_time",
 *   label = @Translation("Schedule Day Time"),
 *   module = "schedule_field",
 *   description = @Translation("Provides possibility to choose day of the week and time."),
 *   default_widget = "schedule_day_time_widget",
 *   default_formatter = "schedule_day_time_formatter"
 * )
 */
class ScheduleDayTime extends FieldItemBase
{

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [];
    foreach (schedule_field_days() as $key => $day) {
      $columns[$key] = [
        'description' => $day,
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ];
      $columns[$key . '_from'] = [
        'description' => t('@day time from', ['@day' => $day]),
        'type' => 'varchar',
        'length' => 5,
        'not null' => TRUE,
        'default' => '',
      ];
      $columns[$key . '_to'] = [
        'description' => t('@day time to', ['@day' => $day]),
        'type' => 'varchar',
        'length' => 5,
        'not null' => TRUE,
        'default' => '',
      ];
    }
    return ['columns' => $columns];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];
    foreach (schedule_field_days() as $key => $day) {
      $properties[$key] = DataDefinition::create('boolean')
        ->setLabel($day);
      $properties[$key . '_from'] = DataDefinition::create('string')
        ->setLabel(t('@day time from', ['@day' => $day]));
      $properties[$key . '_to'] = DataDefinition::create('string')
        ->setLabel(t('@day time to', ['@day' => $day]));
    }
    return $properties;
  }

}
