<?php

namespace Drupal\schedule_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'schedule_day_time' formatter.
 *
 * @FieldFormatter(
 *   id = "schedule_day_time_formatter",
 *   label = @Translation("Schedule days and time"),
 *   field_types = {
 *     "schedule_day_time"
 *   }
 * )
 */
class ScheduleDayTimeFormatter extends FormatterBase
{

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $header = [t('Day'), t('Time from'), t('Time to')];
    foreach ($items as $delta => $item) {
      $rows = [];
      $values = $item->getValue();
      foreach (schedule_field_days() as $key => $day) {
        if (!empty($values[$key]) && !empty($values[$key . '_from']) && !empty($values[$key . '_to'])) {
          $rows[] = [
            $day,
            $values[$key . '_from'],
            $values[$key . '_to'],
          ];
        }
      }

      if (count($rows) > 0) {
        $elements[$delta] = [
          '#type' => 'table',
          '#header' => $header,
          '#rows' => $rows,
        ];
      }
    }
    return $elements;
  }

}
