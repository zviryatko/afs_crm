<?php

namespace Drupal\schedule_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\WidgetBase;

/**
 * Plugin implementation of the 'schedule_day_time' widget.
 *
 * @FieldWidget(
 *   id = "schedule_day_time_widget",
 *   module = "schedule_field",
 *   label = @Translation("Schedule day and time"),
 *   field_types = {
 *     "schedule_day_time"
 *   }
 * )
 */
class ScheduleDayTimeWidget extends WidgetBase
{

  /**
   * Validate the time.
   */
  public function timeValidate($element, FormStateInterface $form_state) {

    $value = $element['#value'];
    $time = new \DateTime;
    $formatted = $time->createFromFormat('H:i', $value);
    $parts = explode(':', $value);
    $group_valid = $this->groupValidate($element, $form_state);
    if (!$group_valid) {
      $form_state->setError($element, t("If day selected time can't be empty and vice versa."));
    }
    if (($formatted !== FALSE && (strlen($parts[0]) == 2) && (strlen($parts[1]) == 2) && ($parts[0] <= 24) && ($parts[1] <= 59))
      || $value === ''
    ) {
      return;
    }
    $form_state->setError($element, t("Time is not valid."));

  }

  /**
   * Validate field group.
   */
  public function groupValidate($element, FormStateInterface $form_state) {
    $field_name = $element['#array_parents'][0];
    $delta = $element['#array_parents'][2];
    $day_name = explode('_', $element['#array_parents'][3]);
    $field_values = $form_state->getValue($field_name);
    $day_value = $field_values[$delta];
    if ((!empty($day_value[$day_name[0]]) && !empty($day_value[$day_name[0] . '_to']) && !empty($day_value[$day_name[0] . '_from'])) ||
      (empty($day_value[$day_name[0]]) && empty($day_value[$day_name[0] . '_to']) && empty($day_value[$day_name[0] . '_from']))
    ) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element = [];
    $values = $items->getValue()[$delta];

    foreach (schedule_field_days() as $key => $day) {
      $element[$key] = [
        '#prefix' => '<div class="day-of-week ' . $key . '">',
        '#title' => $day,
        '#type' => 'checkbox',
        '#default_value' => $values[$key],
      ];
      $element[$key . '_from'] = [
        '#description' => t('Time from'),
        '#type' => 'textfield',
        '#default_value' => $values[$key . '_from'],
        '#size' => 5,
        '#maxlength' => 5,
        '#attributes' => ['placeholder' => '00:00'],
        '#element_validate' => [
          [$this, 'timeValidate'],
        ],
      ];
      $element[$key . '_to'] = [
        '#description' => t('Time to'),
        '#type' => 'textfield',
        '#default_value' => $values[$key . '_to'],
        '#size' => 5,
        '#maxlength' => 5,
        '#attributes' => ['placeholder' => '00:00'],
        '#element_validate' => [
          [$this, 'timeValidate'],
        ],
        '#suffix' => '</div>',
      ];
    }

    return $element;
  }

}
