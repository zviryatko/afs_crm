<?php

namespace Drupal\sa_messages\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Messages entity entities.
 *
 * @ingroup sa_messages
 */
class MessagesEntityDeleteForm extends ContentEntityDeleteForm {


}
