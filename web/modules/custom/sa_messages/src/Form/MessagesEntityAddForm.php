<?php

namespace Drupal\sa_messages\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Messages entity edit forms.
 *
 * @ingroup sa_messages
 */
class MessagesEntityAddForm extends ContentEntityForm {

  public $id;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\sa_messages\Entity\MessagesEntity */
    $form = parent::buildForm($form, $form_state);

    // Add fancy select.
    $form['#attached']['library'][] =  'sa_admin_theme/fancy_select';
    $form['#attached']['library'][] = 'sa_admin_theme/lessons_replace';

    // Additional submit actions.
    $form['actions']['submit_publish'] = $form['actions']['submit'];
    $form['actions']['submit_publish']['#value'] = t('Save and publish');
    $form['actions']['submit_publish']['#submit'][1] = '::publish';
    $form['actions']['submit_publish']['#submit'][2] = '::save';
    $form['actions']['submit_publish']['#submit'][3] = '::tasks_redirect';
    $form['actions']['submit_publish']['#weight'] = '4';
    $form['actions']['submit']['#submit'][1] = '::save';
    $form['actions']['submit']['#submit'][2] = '::tasks_redirect';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;

    // Unset hours and minutes for date.
    $dt = $entity->get('done_timestamp')->value;
    $entity->set('done_timestamp', strtotime(date("Y-m-d", $dt)));

    $status = parent::save($form, $form_state);
    $this->id = $entity->id();
    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Messages entity.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Messages entity.', [
          '%label' => $entity->label(),
        ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function publish(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;
    $entity->set('status', 1);
  }

  /**
   * {@inheritdoc}
   */
  public function tasks_redirect(array $form, FormStateInterface $form_state) {
//    $entity = &$this->entity;
//    $form_state->setRedirect('entity.messages_entity.canonical', ['messages_entity' => $this->id]);
  }

}
