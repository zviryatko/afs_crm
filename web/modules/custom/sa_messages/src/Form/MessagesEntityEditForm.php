<?php

namespace Drupal\sa_messages\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Ajax\ReplaceCommand;

/**
 * Form controller for Messages entity edit forms.
 *
 * @ingroup sa_messages
 */
class MessagesEntityEditForm extends ContentEntityForm
{

  public $id;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\sa_messages\Entity\MessagesEntity */
    $form = parent::buildForm($form, $form_state);

    // Add fancy select.
    $form['#attached']['library'][] = 'sa_admin_theme/fancy_select';
    $form['#attached']['library'][] = 'sa_admin_theme/lessons_replace';

    $entity = &$this->entity;

    // Additional submit actions.
    if ($entity->isDone()) {
      $form['actions']['undone'] = $form['actions']['submit'];
      $form['actions']['undone']['#value'] = t('Save as Undone');
      $form['actions']['undone']['#submit'][1] = '::undone';
      $form['actions']['undone']['#submit'][2] = '::publish';
      $form['actions']['undone']['#submit'][3] = '::save';
      $form['actions']['undone']['#submit'][4] = '::tasks_redirect';
      $form['actions']['undone']['#weight'] = '4';
    } else {
      $form['actions']['done'] = $form['actions']['submit'];
      $form['actions']['done']['#value'] = t('Save as Done');
      $form['actions']['done']['#submit'][1] = '::done';
      $form['actions']['done']['#submit'][2] = '::publish';
      $form['actions']['done']['#submit'][3] = '::save';
      $form['actions']['done']['#submit'][4] = '::tasks_redirect';
      $form['actions']['done']['#weight'] = '4';
    }

    // Update submit.
    $form['actions']['submit']['#submit'][1] = '::publish';
    $form['actions']['submit']['#submit'][2] = '::save';
    $form['actions']['submit']['#submit'][3] = '::tasks_redirect';
    $form['actions']['submit']['#value'] = t('Update publish');

    if ($entity->getOwnerId() != \Drupal::currentUser()->id()) {
      unset($form['actions']['delete']);
      $form['name']['#disabled'] = TRUE;
    } else {
      $form['actions']['unpublish'] = $form['actions']['submit'];
      $form['actions']['unpublish']['#value'] = t('Update unpublish');
      $form['actions']['unpublish']['#submit'][1] = '::unpublish';
      $form['actions']['unpublish']['#submit'][2] = '::save';
      $form['actions']['unpublish']['#submit'][3] = '::tasks_redirect';
      $form['actions']['unpublish']['#weight'] = '6';
    }

    // Add Author and status.
    $user = $entity->getOwner();
    $form['author_info'] = [
      '#prefix' => '<h2>',
      '#markup' => t('Author is @name @surname - @label', [
        '@name' => $user->get('field_first_name')->value,
        '@surname' => $user->get('field_last_name')->value,
        '@label' => $user->label(),
      ]),
      '#suffix' => '</h2',
    ];

    $form['status_info'] = [
      '#prefix' => '<h2>',
      '#markup' => $entity->isDone() ? t('Task is done') : t("Task isn't done"),
      '#suffix' => '</h2',
    ];
    $form['publish_info'] = [
      '#prefix' => '<h2>',
      '#markup' => $entity->isPublished() ? t('Task is published') : t("Task isn't published"),
      '#suffix' => '</h2',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;

    // Unset hours and minutes for date.
    $dt = $entity->get('done_timestamp')->value;
    $entity->set('done_timestamp', strtotime(date("Y-m-d", $dt)));

    $status = parent::save($form, $form_state);
    $this->id = $entity->id();
    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Messages entity.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Messages entity.', [
          '%label' => $entity->label(),
        ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function done(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;
    $entity->set('done', 1);
  }

  /**
   * {@inheritdoc}
   */
  public function undone(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;
    $entity->set('done', 0);
  }

  /**
   * {@inheritdoc}
   */
  public function publish(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;
    $entity->set('status', 1);
  }

  /**
   * {@inheritdoc}
   */
  public function unpublish(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;
    $entity->set('status', 0);
  }

  /**
   * {@inheritdoc}
   */
  public function tasks_redirect(array $form, FormStateInterface $form_state) {
//    $entity = &$this->entity;
//    $form_state->setRedirect('entity.messages_entity.canonical', ['messages_entity' => $this->id]);
  }

}
