<?php

namespace Drupal\sa_messages\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsWidgetBase;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\user\Entity\User;

/**
 * Plugin implementation of the 'user_options_select' widget.
 *
 * @FieldWidget(
 *   id = "user_options_select",
 *   label = @Translation("User Select list"),
 *   field_types = {
 *     "entity_reference",
 *   },
 *   multiple_values = TRUE
 * )
 */
class UserOptionsSelectWidget extends OptionsWidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $roles = [];
    if (!empty($this->getFieldSettings()['handler_settings']['filter']['role'])) {
      $roles = $this->getFieldSettings()['handler_settings']['filter']['role'];
    }

    $element += [
      '#type' => 'select',
      '#options' => $this->getUserOptions($items->getEntity(), $roles),
      '#default_value' => $this->getSelectedOptions($items),
      // Do not display a 'multiple' select box if there is only one option.
      '#multiple' => $this->multiple && count($this->options) > 1,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function sanitizeLabel(&$label) {
    // Select form inputs allow unencoded HTML entities, but no HTML tags.
    $label = Html::decodeEntities(strip_tags($label));
  }

  /**
   * {@inheritdoc}
   */
  protected function supportsGroups() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEmptyLabel() {
    if ($this->multiple) {
      // Multiple select: add a 'none' option for non-required fields.
      if (!$this->required) {
        return t('- None -');
      }
    } else {
      // Single select: add a 'none' option for non-required fields,
      // and a 'select a value' option for required fields that do not come
      // with a value selected.
      if (!$this->required) {
        return t('- None -');
      }
      if (!$this->has_value) {
        return t('- Select a value -');
      }
    }
  }

  /**
   * Returns the array of options for the widget.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity for which to return options.
   *
   * @return array
   *   The array of options for the widget.
   */
  public function getUserOptions(FieldableEntityInterface $entity, $roles = []) {
    $clean_roles = [];
    foreach ($roles as $role) {
      if (empty($role)) {
        continue;
      }
      $clean_roles[] = $role;
    }
    if (empty($clean_roles)) {
      $clean_roles = ['admin', 'student', 'teacher', 'manager'];
    }
    if (!isset($this->options)) {

      $cid = 'students:' . implode('_', $clean_roles);
      $data = NULL;
      if ($cache = \Drupal::cache()->get($cid)) {
        $users = $cache->data;
      } else {
        $query = \Drupal::database()->select('users', 'u');
        $query->fields('u', ['uid']);
        $query->fields('u_d', ['name']);
        $query->fields('u_f_n', ['field_first_name_value']);
        $query->fields('u_l_n', ['field_last_name_value']);
        $query->condition('u_r.roles_target_id', $clean_roles, 'IN');
        $query->condition('u_d.status', 1, '=');
        $query->leftJoin('user__roles', 'u_r', 'u.uid=u_r.entity_id');
        $query->leftJoin('users_field_data', 'u_d', 'u.uid=u_d.uid');
        $query->leftJoin('user__field_first_name', 'u_f_n', 'u.uid=u_f_n.entity_id');
        $query->leftJoin('user__field_last_name', 'u_l_n', 'u.uid=u_l_n.entity_id');
        $result = $query->execute();
        $users = $result->fetchAll();
        \Drupal::cache()->set($cid, $users, time() + (60 * 60 * 24));
      }

      $options = [];
      foreach ($users as $key => $user) {
        $name = $user->field_first_name_value;
        $surname = $user->field_last_name_value;
        $label = $user->name;
        $options[$user->uid] = $name . ' ' . $surname . ' ' . $label;
      }

      // Add 'me'.
      $current_user = \Drupal::currentUser()->id();
      if (isset($users[$current_user])) {
        $options = [$current_user => t('<< me >>')] + $options;
      }

      // Add an empty option if the widget needs one.
      if ($empty_label = $this->getEmptyLabel()) {
        $options = ['_none' => $empty_label] + $options;
      }

      $this->options = $options;
    }
    return $this->options;
  }

}
