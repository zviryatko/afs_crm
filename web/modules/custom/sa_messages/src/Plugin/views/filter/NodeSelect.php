<?php

namespace Drupal\sa_messages\Plugin\views\filter;

use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\InOperator;
use Drupal\node\Entity\Node;

/**
 * Complex filter to handle filtering for many to one relationships,
 * such as terms (many terms per node) or roles (many roles per user).
 *
 * The construct method needs to be overridden to provide a list of options;
 * alternately, the valueForm and adminSummary methods need to be overridden
 * to provide something that isn't just a select list.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("node_select_filter")
 */
class NodeSelect extends InOperator {

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->valueTitle = t('Allowed node titles');
    $this->definition['options callback'] = array($this, 'generateOptions');
  }

  /**
   * Override the query so that no filtering takes place if the user doesn't
   * select any options.
   */
  public function query() {
    if (!empty($this->value)) {
      parent::query();
    }
  }

  /**
   * Skip validation if no options have been chosen so we can use it as a
   * non-filter.
   */
  public function validate() {
    if (!empty($this->value)) {
      parent::validate();
    }
  }

  /**
   * Helper function that generates the options.
   * @return array
   */
  public function generateOptions() {

    $field = str_replace('.', '', $this->getField());
    $entity = $this->getEntityType();

    $db = \Drupal::database();
    $query = $db->select($entity , 'n');
    $query->fields('n', [$field]);
    $result = $query->execute()->fetchAll();

    $nids = [];
    foreach ($result as $item) {
      if (!empty($item->{$field})) {
        $nids[] = $item->{$field};
      }
    }

    $nids_filtered = array_unique($nids);
    $nodes = Node::loadMultiple($nids_filtered);
    $options = [];
    foreach ($nodes as $nid => $node) {
      $options[$nid] = $node->get('title')->value;
    }

    return $options;
  }
}
