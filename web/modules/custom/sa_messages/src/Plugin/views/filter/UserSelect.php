<?php

namespace Drupal\sa_messages\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ManyToOneHelper;
use Drupal\views\Plugin\views\filter\InOperator;

/**
 * Complex filter to handle filtering for many to one relationships,
 * such as terms (many terms per node) or roles (many roles per user).
 *
 * The construct method needs to be overridden to provide a list of options;
 * alternately, the valueForm and adminSummary methods need to be overridden
 * to provide something that isn't just a select list.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("user_select_filter")
 */
class UserSelect extends InOperator {

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->valueTitle = t('Allowed node titles');
    $this->definition['options callback'] = array($this, 'generateOptions');
  }

  /**
   * Override the query so that no filtering takes place if the user doesn't
   * select any options.
   */
  public function query() {
    if (!empty($this->value)) {
      parent::query();
    }
  }

  /**
   * Skip validation if no options have been chosen so we can use it as a
   * non-filter.
   */
  public function validate() {
    if (!empty($this->value)) {
      parent::validate();
    }
  }

  /**
   * Helper function that generates the options.
   * @return array
   */
  public function generateOptions() {

    $field = str_replace('.', '', $this->getField());
    $entity = $this->getEntityType();

    $db = \Drupal::database();
    $query = $db->select($entity , 'n');
    $query->fields('n', [$field]);
    $result = $query->execute()->fetchAll();

    $uids = [];
    foreach ($result as $item) {
      $uids[] = $item->{$field};
    }

    $uids = array_unique($uids);

    $query = $db->select('search_api_db_users' , 'n');
    $query->fields('n', [
      'uid',
      'name',
      'field_first_name',
      'field_last_name',
      'field_patronymic',
      'field_card_number',
    ]);
    if (!empty($uids)) {
      $query->condition('uid', $uids, 'IN');
    }
    $result = $query->execute()->fetchAll();

    $options = [];
    foreach ($result as $item) {
      $options[$item->uid] = $item->field_first_name .
        ' ' . $item->field_last_name . ' ' . $item->field_patronymic .
        ' ' . $item->field_card_number . ' ' . $item->name;
    }

    return $options;
  }
}
