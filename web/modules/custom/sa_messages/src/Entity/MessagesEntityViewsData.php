<?php

namespace Drupal\sa_messages\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Messages entity entities.
 */
class MessagesEntityViewsData extends EntityViewsData
{

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['messages_entity']['priority']['filter'] = [
      'id' => 'message_priority'
    ];

    $data['messages_entity']['done_date']['filter'] = [
      'id' => 'date'
    ];

    $data['messages_entity']['done_timestamp']['filter'] = [
      'id' => 'date'
    ];

    $data['messages_entity']['user_assigned']['filter']['id'] = 'user_select_filter';

    $data['messages_entity']['user_targeted']['filter']['id'] = 'user_select_filter';

    $data['messages_entity']['user_created']['filter']['id'] = 'user_select_filter';

    $data['messages_entity']['class']['filter']['id'] = 'node_select_filter';

    return $data;
  }

}
