<?php

namespace Drupal\sa_messages\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Messages entity entity.
 *
 * @ingroup sa_messages
 *
 * @ContentEntityType(
 *   id = "messages_entity",
 *   label = @Translation("Tasks entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\sa_messages\Entity\MessagesEntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\sa_messages\Form\MessagesEntityForm",
 *       "add" = "Drupal\sa_messages\Form\MessagesEntityAddForm",
 *       "edit" = "Drupal\sa_messages\Form\MessagesEntityEditForm",
 *       "delete" = "Drupal\sa_messages\Form\MessagesEntityDeleteForm",
 *     },
 *     "access" = "Drupal\sa_messages\MessagesEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\sa_messages\MessagesEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "messages_entity",
 *   admin_permission = "administer messages entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/messages_entity/{messages_entity}",
 *     "add-form" = "/admin/structure/messages_entity/add",
 *     "edit-form" = "/admin/structure/messages_entity/{messages_entity}/edit",
 *     "delete-form" = "/admin/structure/messages_entity/{messages_entity}/delete",
 *   },
 *   field_ui_base_route = "messages_entity.settings"
 * )
 */
class MessagesEntity extends ContentEntityBase implements MessagesEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_created' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_created')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_created')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_created', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_created', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTarget() {
    return $this->get('user_targeted')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetId() {
    return $this->get('user_targeted')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setTargetId($uid) {
    $this->set('user_targeted', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setTarget(UserInterface $account) {
    $this->set('user_targeted', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return !empty($this->get('status')->value) ? TRUE : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isDone() {
    return !empty($this->get('done')->value) ? TRUE : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setDone($done) {
    $this->set('done', $done ? 1 : 0);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the task entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Message entity.'))
      ->setReadOnly(TRUE);

    $fields['user_created'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the task.'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['user_assigned'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Assign to'))
      ->setDescription(t('The user ID who should do this task.'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['user_targeted'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Targeted user'))
      ->setDescription(t('The user ID to which action needed.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['message'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Message'))
      ->setDescription(t('A body of the message.'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'text_default',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The name of the Messages entity entity.'))
      ->setRequired(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['priority'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Priority'))
      ->setDescription(t('Priority on this task/message.'))
      ->setSetting('allowed_values', [
        'urgent' => t('Urgent'),
        'high' => t('High'),
        'normal' => t('Normal'),
        'low' => t('Low'),
      ])
      ->setRequired(TRUE)
      ->setCardinality(1)
      ->setDefaultValue('normal')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -3,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'list_default',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['class'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Class'))
      ->setSetting('target_type', 'node')
      ->setCardinality(1)
      ->setSetting('handler_settings', ['target_bundles' => ['classes']])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'label',
        'weight' => 4,
        'settings' => [
          'link' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['lesson'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Lesson'))
      ->setSetting('target_type', 'node')
      ->setCardinality(1)
      ->setSetting('handler_settings', ['target_bundles' => ['lesson']])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 4.5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'label',
        'weight' => 4.5,
        'settings' => [
          'link' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['done'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Done status'))
      ->setDescription(t('A boolean indicating whether the task is Done.'))
      ->setDefaultValue(FALSE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the task entity is published.'))
      ->setDefaultValue(FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['done_timestamp'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Date'))
      ->setDescription(t('Till what date task should be done.'))
      ->setRequired(TRUE)
      ->setSettings([
        'datetime_type' => 'date',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);



    return $fields;
  }

}
