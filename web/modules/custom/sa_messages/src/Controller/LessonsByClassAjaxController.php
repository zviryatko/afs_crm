<?php

namespace Drupal\sa_messages\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\BaseCommand;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller routines for AJAX route.
 */
class LessonsByClassAjaxController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  protected function getModuleName() {
    return 'sa_messages';
  }

  /**
   * Callback for link.
   * @param $class
   * @return AjaxResponse|Response
   */
  public function loadLessonsCallback($class = NULL) {
    try {
      $response = new JsonResponse();
      $query = \Drupal::entityQuery('node')
        ->condition('status', 1)
        ->condition('type', 'lesson', '=')
        ->condition('field_class', $class, '=');
      $nids = $query->execute();
      $response->setData(array_values($nids));
      return $response;
    }
    catch (\Exception $e) {
      watchdog_exception('sa_messages', $e);
      $response = new Response($this->t("Something went wrong."));
      return $response;
    }
  }
}
