<?php

/**
 * @file
 * Contains \Drupal\sa_cron_queue\Plugin\QueueWorker\TeacherLessonSalaryQueueWorker.
 */

namespace Drupal\sa_cron_queue\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\node\NodeStorageInterface;
use Drupal\taxonomy\TermStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Teacher Lesson Salary Queue, creates payments and/or tasks for managers.
 *
 * @QueueWorker(
 *   id = "teacher_lesson_salary_queue",
 *   title = @Translation("Teacher Lesson Salary Queue"),
 *   cron = {"time" = 10}
 * )
 */
class TeacherLessonSalaryQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The node storage.
   *
   * @var \Drupal\node\NodeStorageInterface
   */
  protected $nodeStorage;

  /**
   * The term storage.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * The term storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $expensesStorage;

  /**
   * The term storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $taskStorage;

  /**
   * The visits storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $visitsStorage;

  /**
   * Creates a new NodePublishBase object.
   *
   * @param \Drupal\node\NodeStorageInterface $nodeStorage
   *   The node storage.
   *
   * @param \Drupal\taxonomy\TermStorageInterface $termStorage
   *   The term storage.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $expensesStorage
   *   The expenses storage.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $taskStorage
   *   The task storage.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $visitsStorage
   *   The visits storage.
   *
   */
  public function __construct(NodeStorageInterface $nodeStorage, TermStorageInterface $termStorage, EntityStorageInterface $expensesStorage, EntityStorageInterface $taskStorage, EntityStorageInterface $visitsStorage) {
    $this->nodeStorage = $nodeStorage;
    $this->termStorage = $termStorage;
    $this->expensesStorage = $expensesStorage;
    $this->taskStorage = $taskStorage;
    $this->visitsStorage = $visitsStorage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('entity.manager')->getStorage('node'),
      $container->get('entity.manager')->getStorage('taxonomy_term'),
      $container->get('entity.manager')->getStorage('expenses_entity'),
      $container->get('entity.manager')->getStorage('messages_entity'),
      $container->get('entity.manager')->getStorage('visits_entity')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($node) {

    // Load node.
    $lesson = $this->nodeStorage->load($node);
    // Class.
    $class = $this->nodeStorage->load($lesson->get('field_class')->target_id);
    // Branch.
    $branch = $this->termStorage->load($class->get('field_branch')->target_id);
    // Check if we need co create payment
    if ($this->paymentNeeded($class)) {
      // Create payment.
      $this->createPayment($class, $lesson, $branch);
    }
    // Create tasks fo users.
    else {
      // Load managers.
      $i = 0;
      foreach ($branch->get('field_managers_payment_messages') as $manager) {
        $values = [
          'user_created' => 1,
          'user_assigned' => $manager->target_id,
          'name' => t('Teacher lesson payment needed'),
          'priority' => 'normal',
          'class' => $class->id(),
          'lesson' => $lesson->id(),
          'status' => 1,
          'done_timestamp' => time() + (3600 * 24 * 7),
        ];
        if ($i > 0) {
          $values['name'] .= ' ' . t('(copy)');
        }
        // Create task for each teacher.
        foreach ($lesson->get('field_teacher_on_lesson') as $user) {
          $values['user_targeted'] = $user->target_id;
          $this->taskStorage->create($values)->save();
        }

        $i++;
      }
    }

    // Check if all users from lesson has visit.
    // Select visits with current lesson.
    // If count not equal to users, create task.
    $lesson_visits = count($this->visitsStorage->loadByProperties(['lesson' => $lesson->id()]));
    $class_students = count($class->get('field_students'));
    $lesson_students = ($lesson->get('field_students'));
    if ($lesson_visits != ($class_students + $lesson_students)) {
      // Load managers.
      $i = 0;
      foreach ($branch->get('field_managers_payment_messages') as $manager) {
        $values = [
          'user_created' => 1,
          'user_assigned' => $manager->target_id,
          'name' => t('Not all users were noted for a lesson.'),
          'priority' => 'normal',
          'class' => $class->id(),
          'lesson' => $lesson->id(),
          'status' => 1,
          'done_timestamp' => time() + (3600 * 24 * 7),
        ];
        if ($i > 0) {
          $values['name'] .= ' ' . t('(copy)');
        }
        $this->taskStorage->create($values)->save();
        $i++;
      }
    }

    // Mark as processed.
    $lesson->set('sticky', 1)->save();
  }

  /**
   * Is payment needed.
   *
   * @param NodeInterface $class
   * @return bool
   */
  function paymentNeeded(NodeInterface $class) {
    $teacher_payment_type = $class->get('field_teacher_payment')->type;
    if ($teacher_payment_type == 'static' || $teacher_payment_type == 'students_depends') {
      return TRUE;
    }
    return FALSE;
  }

  function createPayment($class, $lesson, $branch) {
    $cats = [];
    foreach ($branch->get('field_auto_teacher_payments') as $cat) {
      $cats[] = $cat->target_id;
    }

    // Set variables for a expenses.
    $values = [
      'status' => 'draft',
      'name' => t('Teacher payment'),
      'field_branch' => $branch->id(),
      'field_class' => $class->id(),
      'field_comment' => t('Payment created automatically depends on a class config.'),
      'field_expenses_category' => $cats,
      'field_room' => $class->get('field_room')->target_id,
      'field_lesson' => $lesson->id(),
      'user_id' =>  1,
      'date' => time(),
    ];

    // Static payment peer lesson.
    $teacher_payment_type = $class->get('field_teacher_payment')->type;
    if ($teacher_payment_type == 'static') {
      $values['field_amount'] = $class->get('field_teacher_payment')->amount;
    }
    // Amount depends on a students.
    elseif ($teacher_payment_type == 'students_depends') {
      $definitions = json_decode($class->get('field_teacher_payment')->definitions);
      $students_visits = $this->visitsStorage->loadByProperties(['lesson' => $lesson->id()]);
      if (isset($definitions->{count($students_visits)})) {
        $values['field_amount'] = $definitions->{count($students_visits)};
      }
      else{
        $values['field_amount'] = $definitions->any;
      }
    }
    // Create payment for each teacher.
    foreach ($lesson->get('field_teacher_on_lesson') as $user) {
      $values['field_user'] = $user->target_id;
      $this->expensesStorage->create($values)->save();
    }
  }

}
