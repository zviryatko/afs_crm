<?php

namespace Drupal\sa_cron_queue\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\node\Entity\Node;

/**
 * Class ImportData.
 *
 * @package Drupal\sa_data_import\Form
 */
class TaskSettingsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'queue_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $date_format = DateFormat::load('html_date')->getPattern();
    $date = new DrupalDateTime();
    $default_value = $date->format($date_format);
    $form['date'] = [
      '#type' => 'date',
      '#default_value' => $default_value,
      '#date_year_range' => '1902:2037',
      '#title' => $this->t('Select needed date, all Lesson before this date will be set as processes. Teacher salary will not be generated.'),
      '#weight' => 90,
    ];

    $form['delete'] = [
      '#type' => 'submit',
      '#value' => $this->t('Process'),
      '#submit' => ['::processLessons'],
      '#weight' => 100,
    ];

    $form['clear_description'] = [
      '#markup' => '<h5>' . $this->t('Delete all queues for teacher salary.') . '</h5>',
      '#weight' => 9,
    ];

    $form['clear'] = array(
      '#type' => 'submit',
      '#value' => t('Clear queue'),
      '#submit' => ['::clearQueue'],
      '#weight' => 10,
    );

    return $form;
  }

  public function clearQueue(array &$form, FormStateInterface $form_state) {
    // Delete queue.
    \Drupal::queue('teacher_lesson_salary_queue')->deleteQueue();
    $messenger = \Drupal::messenger();
    $messenger->addMessage(t("Queue cleared."));
  }


  public function processLessons(array &$form, FormStateInterface $form_state) {

    $date = $form_state->getValue('date');
    $timestamp = strtotime($date);
    // Select Lessons.
    $query = \Drupal::entityQuery('node');
    $query->condition('status', 1);
    $query->condition('sticky', 0);
    $query->condition('type', 'lesson');
    $query->condition('field_lesson_to_time', $timestamp, "<");
    $entity_ids = $query->execute();

    $i = 0;
    foreach ($entity_ids as $id) {
      Node::load($id)->setSticky(TRUE)->save();
      $i++;
    }
    $messenger = \Drupal::messenger();
    $messenger->addMessage(t("@n lessons processed.", ['@n' => $i]));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
