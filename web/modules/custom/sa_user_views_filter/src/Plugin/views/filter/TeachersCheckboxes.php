<?php
/**
 * @file
 * Definition of Drupal\sa_user_views_filter\Plugin\views\filter\TeachersCheckboxes.
 */
namespace Drupal\sa_user_views_filter\Plugin\views\filter;

use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\InOperator;
use Drupal\views\ViewExecutable;
use Drupal\user\Entity\User;
use Drupal\Core\Form\FormStateInterface;

/**
 * Filters by given list of related content title options.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("teachers_checkboxes")
 */
class TeachersCheckboxes extends InOperator {

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->valueTitle = t('Allowed node titles');
    $this->definition['options callback'] = array($this, 'generateOptions');
    $this->valueFormType = 'select';
  }

  /**
   * Override the query so that no filtering takes place if the user doesn't
   * select any options.
   */
  public function query() {
    if (!empty($this->value)) {
      parent::query();
    }
  }

  /**
   * Skip validation if no options have been chosen so we can use it as a
   * non-filter.
   */
  public function validate() {
    if (!empty($this->value)) {
      parent::validate();
    }
  }

  /**
   * Helper function that generates the options.
   * @return array
   */
  public function generateOptions() {
        $ids = \Drupal::entityQuery('user')
      ->condition('status', 1)
      ->condition('roles', 'teacher')
      ->execute();
    $users = User::loadMultiple($ids);

    $options = [];

    foreach ($users as $key => $user) {
      $name = $user->get('field_first_name')->value;
      $surname = $user->get('field_last_name')->value;
      $patr = $user->get('field_patronymic')->value;
      $label = $user->label();
      $options[$key] = $name . ' ' . $surname . ' ' . $patr . ' ' . $label;
    }
    return $options;
  }

}