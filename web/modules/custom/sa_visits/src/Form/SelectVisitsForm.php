<?php
/**
 * @file
 * Contains \Drupal\sa_visits\Form\SelectVisitsForm.
 */

namespace Drupal\sa_visits\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;

/**
 * Contribute form.
 */
class SelectVisitsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {

    // Get raw microtime (with spaces and dots and digits).
    $mt = microtime();

    // Remove all non-digit (or non-integer) characters.
    $r = "";
    $length = strlen($mt);
    for ($i = 0; $i < $length; $i++) {
      if (ctype_digit($mt[$i])) {
        $r .= $mt[$i];
      }
    }

    // Return.
    return $r;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $visit = NULL, $parameters = NULL) {

    $id = $visit->id();
    if (!isset($id)) {
      // Visit entity not given, return.
      return $form;
    }

    // Form generals.
    $lesson = $visit->getLesson();
    $status = $visit->getStatus();
    $changed = $visit->getChangedTime();

    $href = Url::fromRoute('sa_visits.ajax_link_callback', ['id' => $visit->id()]);
    $form['#attributes'] = [
      'data-href' => $href->toString(),
    ];
    $form['#attached'] = [
      'library' => [
        'sa_admin_theme/visits',
      ],
    ];

    // Parameters not given, regular load.
    if (empty($parameters)) {

      $form['#prefix'] = '<div class="visit-form-wrapper ' . $status . '" id="form-wrapper-id-' . $visit->id() . '">';
      $form['#suffix'] = '</div>';

      $default_date = date('Y-m-d');
      $lesson_options = [];
      if (!empty($lesson) && !empty($status) && !empty($changed)) {
        // Lesson.
        $lesson_loaded = Node::load($lesson);
        $lesson_class = Node::load($lesson_loaded->get('field_class')->target_id);
        $lesson_class_title = $lesson_class->getTitle();
        $lesson_options[$lesson] = $lesson_loaded->getTitle() . ' - ' . $lesson_class_title;
        $default_lesson = $lesson;
        // Date.
        $date_time = \DateTime::createFromFormat('U', $changed);
        $default_date = $date_time->format('Y-m-d');
        // Status.
        $default_status = $status;
        $form['status'] = $this->formStatus($default_status);
        $form['lesson'] = $this->formLesson($lesson_options, $default_lesson);
      } else {
        $timestamp = date('U');

        // Load component.
        $component = $visit->get('component')->target_id;
        $block = \Drupal::entityTypeManager()->getStorage('block_content')->load($component);
        $limit = $block->get('field_lesson_limit')->value;
        switch ($limit) {
          case 'by_class':
            $loaded_lessons = $this->lessonsByClass($timestamp, $block);
            $default_lesson = '';
            foreach ($loaded_lessons as $key => $value) {
              $lesson_class = Node::load($value->get('field_class')->target_id);
              $lesson_class_title = $lesson_class->getTitle();
              $lesson_options[$key] = $value->getTitle() . ' - ' . $lesson_class_title;
              $default_lesson = $key;
            }
            break;

          case "by_category":
            $loaded_lessons = $this->lessonsByCategory($timestamp, $block);
            $default_lesson = '';
            foreach ($loaded_lessons as $key => $value) {
              $lesson_class = Node::load($value->get('field_class')->target_id);
              $lesson_class_title = $lesson_class->getTitle();
              $lesson_options[$key] = $value->getTitle() . ' - ' . $lesson_class_title;
              $default_lesson = $key;
            }
            break;
        }
        // There are lessons for today.
        if (!empty($lesson_options)) {
          $form['status'] = $this->formStatus();
          $form['lesson'] = $this->formLesson($lesson_options, $default_lesson);
        } else {
          $form['help'] = $this->formHelp();
        }
      }

      // Show date anyway.
      $form['date'] = $this->formDate($default_date);


    } // Parameters given, ajax load.
    else {
      $form['#prefix'] = '<div class="visit-form-wrapper" id="form-wrapper-id-' . $visit->id() . '">';
      $form['#suffix'] = '</div>';

      $date = $parameters['date'];
      $dtime = \DateTime::createFromFormat('Y-m-d', $date);
      $timestamp = $dtime->format('U');

      // Load component.
      $component = $visit->get('component')->target_id;
      $block = \Drupal::entityTypeManager()->getStorage('block_content')->load($component);
      $limit = $block->get('field_lesson_limit')->value;
      switch ($limit) {
        case 'by_class':
          $loaded_lessons = $this->lessonsByClass($timestamp, $block);
          $default_lesson = '';
          foreach ($loaded_lessons as $key => $value) {
            $lesson_class = Node::load($value->get('field_class')->target_id);
            $lesson_class_title = $lesson_class->getTitle();
            $lesson_options[$key] = $value->getTitle() . ' - ' . $lesson_class_title;
            $default_lesson = $key;
          }
          break;

        case "by_category":
          $loaded_lessons = $this->lessonsByCategory($timestamp, $block);
          $default_lesson = '';
          foreach ($loaded_lessons as $key => $value) {
            $lesson_class = Node::load($value->get('field_class')->target_id);
            $lesson_class_title = $lesson_class->getTitle();
            $lesson_options[$key] = $value->getTitle() . ' - ' . $lesson_class_title;
            $default_lesson = $key;
          }
          break;
      }
      // There are lessons for today.
      if (!empty($lesson_options)) {
        $form['status'] = $this->formStatus();
        $form['lesson'] = $this->formLesson($lesson_options, $default_lesson);
      } else {
        $form['help'] = $this->formHelp();
      }

      // Show date anyway.
      $form['date'] = $this->formDate($date);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Nothing to do here.
  }

  /**
   * Return form date.
   * @param $date
   * @return array
   */
  public function formDate($date) {
    $element = [
      '#type' => 'date',
      '#default_value' => $date,
      '#attributes' => [
        'type' => 'date',
        'placeholder' => 'yy-mm-dd',
      ],
      '#weight' => 10
    ];
    return $element;
  }

  /**
   * Return form lesson.
   * @param $lesson_options
   * @param $default_lesson
   * @return array
   */
  public function formLesson($lesson_options, $default_lesson) {
    $element = [
      '#type' => 'select',
      '#options' => $lesson_options,
      '#default_value' => $default_lesson,
      '#weight' => 30
    ];
    return $element;
  }

  /**
   * Return form help.
   * @return array
   */
  public function formHelp() {
    $element = [
      '#markup' => $this->t('No lessons for selected day, change date.'),
      '#weight' => 50,
      '#prefix' => '<div class="visit-message">',
      '#suffix' => '</div>',
    ];
    return $element;
  }

  /**
   * Return form status.
   * @param $default
   * @return array
   */
  public function formStatus($default = '_none') {
    $status_options = [
      'visited' => t('Visited'),
      'missed' => t('Missed'),
      'postponed' => t('Postponed'),
      '_none' => t('None')
    ];
    $element = [
      '#type' => 'select',
      '#options' => $status_options,
      '#default_value' => $default,
      '#weight' => 40
    ];
    return $element;
  }

  /**
   * Load lessons by category.
   * @param $timestamp
   * @param $block
   * @return array
   */
  public function lessonsByCategory($timestamp, $block) {
    $yesterday = strtotime('-1 day', $timestamp);
    $tomorrow = strtotime('+1 day', $timestamp);
    $categories = $block->get('field_categories');
    $categories_ids = [];
    foreach ($categories as $category) {
      $categories_ids[] = $category->target_id;
    }
    $query = \Drupal::entityQuery('node');
    $query->condition('status', 1);
    $query->condition('type', 'classes');
    $query->condition('field_category', $categories_ids, 'IN');
    $classes_ids = $query->execute();
    // No classes added.
    if (empty($classes_ids)) {
      return [];
    }
    $query = \Drupal::entityQuery('node');
    $query->condition('status', 1);
    $query->condition('type', 'lesson');
    $query->condition('field_class', $classes_ids, 'IN');
    $query->condition('field_lesson_from_time', [$yesterday, $tomorrow], 'BETWEEN');
    $lessons = $query->execute();
    $loaded_lessons = Node::loadMultiple($lessons);
    return $loaded_lessons;
  }

  /**
   * Load lessons by class.
   * @param $timestamp
   * @param $block
   * @return array
   */
  public function lessonsByClass($timestamp, $block) {
    $yesterday = strtotime('-1 day', $timestamp);
    $tomorrow = strtotime('+1 day', $timestamp);
    $classes = $block->get('field_classes');
    $classes_ids = [];
    foreach ($classes as $class) {
      $classes_ids[] = $class->target_id;
    }
    $query = \Drupal::entityQuery('node');
    $query->condition('status', 1);
    $query->condition('type', 'lesson');
    $query->condition('field_class', $classes_ids, 'IN');
    $query->condition('field_lesson_from_time', [$yesterday, $tomorrow], 'BETWEEN');
    $lessons = $query->execute();
    $loaded_lessons = Node::loadMultiple($lessons);
    return $loaded_lessons;
  }

}
