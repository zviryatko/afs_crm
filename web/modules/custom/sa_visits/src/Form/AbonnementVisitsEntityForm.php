<?php

namespace Drupal\sa_visits\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;

/**
 * Form controller for Visits entity edit forms.
 *
 * @ingroup sa_visits
 */
class AbonnementVisitsEntityForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {

    $entity = $this->entity;

    return 'select_visits_form_' . $entity->id();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\sa_visits\Entity\VisitsEntity */
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    $form['#prefix'] = '<div id="form-wrapper-id-' . $entity->id() . '">';
    $form['#suffix'] = '</div>';

    $form['name']['#access'] = FALSE;
    $form['payment']['#access'] = FALSE;
    $form['component']['#access'] = FALSE;
    $form['actions']['#access'] = FALSE;

    $form['status']['#disabled'] = TRUE;

    $form['date-' . $entity->id()] = [
      '#type' => 'date',
      '#ajax' => [
        'callback' => '::visitRebuild',
        'wrapper' => 'form-wrapper-id-' . $entity->id(),
      ],
      //'#default_value' => $default_date,
      '#attributes' => [
        'type' => 'date',
        'placeholder' => 'yy-mm-dd',
      ],
      '#required' => TRUE,
    ];
//
//    $form['submit-' . $entity->id()] = [
//      '#type' => 'submit',
//      '#value' => $this->t('Save'),
//      '#attributes' => [],
//      '#ajax' => [
//          'callback' => '::visitSubmitValidation',
//          'wrapper' => 'form-wrapper-id-' . $entity->id(),
//      ],
//      '#weight' => 50,
//    ];

    $form['submit-' . $entity->id()] = [
      '#markup' => 'sdfsdfsdf',
      '#weight' => 50,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    ///parent::save($form, $form_state);

  }

//  public function visitRebuild(array $form, FormStateInterface $form_state) {
//
//    $entity = $this->entity;
//
//
//    $form_state->setValue('bla', 'sdf');
//    $form_state->setRebuild(TRUE);
//
////    $renderer = \Drupal::service('renderer');
////    $response = new AjaxResponse();
////    $response->addCommand(new ReplaceCommand('#select-visits-form-' . $entity->id(), $renderer->render($form)));
////    return $response;
//
//    $form['status']['#disabled'] = FALSE;
//
//    unset($form['status']);
//    $bla = \Drupal::service('renderer')->render($form);
//
//    //return $bla;
//
//  }
//
//  public function visitSubmitValidation(array $form, FormStateInterface $form_state) {
//
//    //$this->save($form, $form_state);
//
//
//    unset($form['status']);
//     $form_state['values']['bla'] = 45;
//     $form_state->setValue('bla', 'sdf');
//    $form_state->setRebuild(TRUE);
//
//
//    return $form;
//  }


}
