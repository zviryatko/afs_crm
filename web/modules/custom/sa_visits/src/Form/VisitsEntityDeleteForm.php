<?php

namespace Drupal\sa_visits\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Visits entity entities.
 *
 * @ingroup sa_visits
 */
class VisitsEntityDeleteForm extends ContentEntityDeleteForm {


}
