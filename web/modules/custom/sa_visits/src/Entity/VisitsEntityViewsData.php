<?php

namespace Drupal\sa_visits\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Visits entity entities.
 */
class VisitsEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
