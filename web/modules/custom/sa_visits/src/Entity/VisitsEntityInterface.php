<?php

namespace Drupal\sa_visits\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Visits entity entities.
 *
 * @ingroup sa_visits
 */
interface VisitsEntityInterface extends  ContentEntityInterface, EntityChangedInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Visits entity name.
   *
   * @return string
   *   Name of the Visits entity.
   */
  public function getName();

  /**
   * Sets the Visits entity name.
   *
   * @param string $name
   *   The Visits entity name.
   *
   * @return \Drupal\sa_visits\Entity\VisitsEntityInterface
   *   The called Visits entity entity.
   */
  public function setName($name);

  /**
   * Gets the Visits entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Visits entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Visits entity creation timestamp.
   *
   * @param int $timestamp
   *   The Visits entity creation timestamp.
   *
   * @return \Drupal\sa_visits\Entity\VisitsEntityInterface
   *   The called Visits entity entity.
   */
  public function setCreatedTime($timestamp);

}
