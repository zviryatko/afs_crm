<?php

namespace Drupal\sa_visits\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Visits entity entity.
 *
 * @ingroup sa_visits
 *
 * @ContentEntityType(
 *   id = "visits_entity",
 *   label = @Translation("Visits entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\sa_visits\VisitsEntityListBuilder",
 *     "views_data" = "Drupal\sa_visits\Entity\VisitsEntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\sa_visits\Form\VisitsEntityForm",
 *       "add" = "Drupal\sa_visits\Form\VisitsEntityForm",
 *       "edit" = "Drupal\sa_visits\Form\VisitsEntityForm",
 *       "delete" = "Drupal\sa_visits\Form\VisitsEntityDeleteForm",
 *       "abonnement" = "Drupal\sa_visits\Form\AbonnementVisitsEntityForm",
 *     },
 *     "access" = "Drupal\sa_visits\VisitsEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\sa_visits\VisitsEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "visits_entity",
 *   admin_permission = "administer visits entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/visits_entity/add",
 *     "edit-form" = "/admin/structure/visits_entity/{visits_entity}/edit",
 *     "delete-form" = "/admin/structure/visits_entity/{visits_entity}/delete",
 *     "collection" = "/admin/structure/visits_entity",
 *   },
 *   field_ui_base_route = "visits_entity.settings"
 * )
 */
class VisitsEntity extends ContentEntityBase implements VisitsEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }


  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLesson() {
    return $this->get('lesson')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('ID'))
      ->setDescription(t('The name of the Visits entity entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['payment'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Payment'))
      ->setDescription(t('The payment of this visit.'))
      ->setSetting('target_type', 'profits_entity')
      ->setRequired(TRUE)
      ->setCardinality(1)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'label',
        'weight' => -3,
        'settings' => [
          'link' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['lesson'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Lesson'))
      ->setDescription(t('Visited lesson.'))
      ->setSetting('target_type', 'node')
      ->setRequired(TRUE)
      ->setCardinality(1)
      ->setSetting('handler_settings', ['target_bundles' => ['lesson']])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'label',
        'weight' => -3,
        'settings' => [
          'link' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['component'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Component'))
      ->setDescription(t('Abonnement component.'))
      ->setSetting('target_type', 'block_content')
      ->setRequired(TRUE)
      ->setCardinality(1)
      ->setSetting('handler_settings', ['target_bundles' => ['abonnement_component']])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'label',
        'weight' => -3,
        'settings' => [
          'link' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Status'))
      ->setDescription(t('Does student visited class.'))
      ->setSetting('allowed_values', [
        'visited' => t('Visited'),
        'missed' => t('Missed'),
        'postponed' => t('Postponed'),
      ])
      ->setCardinality(1)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -3,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'list_default',
        'weight' => -3,
      ])
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
