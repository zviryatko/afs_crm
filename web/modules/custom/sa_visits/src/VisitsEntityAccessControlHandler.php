<?php

namespace Drupal\sa_visits;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Visits entity entity.
 *
 * @see \Drupal\sa_visits\Entity\VisitsEntity.
 */
class VisitsEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\sa_visits\Entity\VisitsEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished visits entity entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published visits entity entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit visits entity entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete visits entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add visits entity entities');
  }

}
