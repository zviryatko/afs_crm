<?php

namespace Drupal\sa_visits\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\BaseCommand;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller routines for AJAX route.
 */
class SaveUpdateVisitAjaxController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  protected function getModuleName() {
    return 'sa_visits';
  }

  /**
   * Callback for link.
   * @param bool $id
   * @param bool $name
   * @param bool $date
   * @param bool $lesson
   * @param bool $status
   * @return AjaxResponse|Response
   */
  public function ajaxLinkCallback($id = FALSE, $name = FALSE, $date = FALSE, $lesson = FALSE, $status = FALSE) {
    try {
      // Get and check parameters.
      if ($id && $name && $date) {
        $entity = \Drupal::entityTypeManager()->getStorage('visits_entity')->load($id);
        $response = new AjaxResponse();
        // If all data given.
        if ($lesson && $status && ($name == 'status') && is_numeric($lesson)) {
          if ($status == '_none') {
            $entity->set('lesson', NULL);
            $entity->set('status', NULL);
          } else {
            $entity->set('lesson', $lesson);
            $entity->set('status', $status);
          }
          $entity->save();
          // TODO: Make it ajax.
          // $response->addCommand(new InvokeCommand('#form-wrapper-id-' . $id, 'removeClass', []));
          // $response->addCommand(new InvokeCommand('#form-wrapper-id-' . $id, 'addClass', ['visit-form-wrapper']));
          // $response->addCommand(new InvokeCommand('#form-wrapper-id-' . $id, 'addClass', [$status]));
          $response->addCommand(new BaseCommand('reloadPage', ''));
          return $response;
        }
        // Reload form.
        $visit_form = \Drupal::formBuilder()
          ->getForm('Drupal\sa_visits\Form\SelectVisitsForm', $entity, ['name' => $name, 'date' => $date]);
        $response->addCommand(new ReplaceCommand('#form-wrapper-id-' . $id, $visit_form));
        return $response;
      }
    } catch (\Exception $e) {
      $response = new Response($this->t("Some parameters are not given."));
      return $response;
    }
  }

  /**
   * @param int $id
   *   Visit ID.
   * @param int $lesson
   *   Lesson ID.
   * @param string $status
   *   Status string.
   * @return AjaxResponse|Response
   */
  public function ajaxLinkLessonCallback($id = NULL, $lesson = NULL, $status = '') {
    try {
      $entity = \Drupal::entityTypeManager()->getStorage('visits_entity')->load($id);
      if ($status == 'none') {
        $entity->set('lesson', NULL);
        $entity->set('status', NULL);
      } else {
        $entity->set('lesson', $lesson);
        $entity->set('status', $status);
      }
      $entity->save();
      // If this component is unlimited, add new visit.
      $block = \Drupal::entityTypeManager()->getStorage('block_content')->load($entity->get('component')->target_id);
      if (!empty($block)) {
        $lesson_limit = $block->get('field_lessons_count')->value;
        // If lesson limit 0 check if we need to add visit.
        if ($lesson_limit == 0) {
          $payment_id = $entity->get('payment')->target_id;
          $component_id = $entity->get('component')->target_id;
          $query = \Drupal::database()->select('visits_entity', 'v_e');
          $query->fields('v_e', ['id']);
          $query->condition('v_e.component', $component_id, '=');
          $query->condition('v_e.payment', $payment_id, '=');
          $query->condition('v_e.lesson', NULL, 'IS NULL');
          $query->condition('v_e.status', NULL, 'IS NULL');
          $query->range(0, 1);
          $result = $query->execute();
          $visits = $result->fetchAll();
          if (empty($visits)) {
            $query = \Drupal::database()->select('visits_entity', 'v_e');
            $query->fields('v_e', ['name']);
            $query->condition('v_e.component', $component_id, '=');
            $query->condition('v_e.payment', $payment_id, '=');
            $query->range(0, 1);
            $query->orderBy('name');
            $result = $query->execute();
            $max_name = reset($result->fetchAll())->name;
            $entity = \Drupal::entityTypeManager()->getStorage('visits_entity')->create();
            $entity->set('payment', $payment_id);
            $entity->set('component', $component_id);
            $entity->set('name', $max_name + 1);
            $entity->save();
          }
        }
      }

      $response = new AjaxResponse();
      $response->addCommand(new InvokeCommand('.lesson-users-list .row-' . $id . ' .link', 'removeClass', ['active']));
      $response->addCommand(new InvokeCommand('.lesson-users-list .row-' . $id . ' .' . $status . '-' . $id, 'addClass', ['active']));
      return $response;

    } catch (\Exception $e) {
      $response = new Response($this->t("Something wend wrong."));
      return $response;
    }
  }

  public function ajaxAddVisit($payment, $component, $name) {
    $entity = \Drupal::entityTypeManager()->getStorage('visits_entity')->create();
    $entity->set('payment', $payment);
    $entity->set('component', $component);
    $entity->set('name', $name);
    $result = $entity->save();
    $response = new AjaxResponse();
    if ($result == SAVED_NEW) {
      $response->addCommand(new BaseCommand('reloadPage', ''));
      return $response;
    }
    $response = new Response($this->t("Something wend wrong."));
    return $response;
  }
}
