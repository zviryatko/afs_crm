<?php

/**
 * @file
 * Contains visits_entity.page.inc.
 *
 * Page callback for Visits entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Visits entity templates.
 *
 * Default template: visits_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_visits_entity(array &$variables) {
  // Fetch VisitsEntity Entity Object.
  $visits_entity = $variables['elements']['#visits_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
