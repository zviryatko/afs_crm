<?php

namespace Drupal\sa_data_import\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\Core\Url;
use Drupal\user\Entity\User;

/**
 * Class ImportData.
 *
 * @package Drupal\sa_data_import\Form
 */
class ImportData extends FormBase {

  /**
   * Columns
   */
  const first_name = 0;
  const last_name = 1;
  const patronymic = 2;
  const date_of_birth = 3;
  const email = 4;
  const phone = 5;
  const disabled = 6;
  const source = 7;
  const status = 8;
  const card = 9;
  const category = 10;
  const comment = 11;
  const gender = 12;
  const child = 13;
  const parent_name = 14;
  const parent_email = 15;
  const parent_phone = 16;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'import_data';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

//    $roles = [
//      'student' => $this->t('Students'),
//      'teacher' => $this->t('Teachers'),
//    ];
//
//    $form['roles'] = [
//      '#type' => 'radios',
//      '#title' => $this->t('Type of users to import'),
//      '#required' => TRUE,
//      '#options' => $roles,
//      '#default_value' => 'student',
//    ];

    $form['download'] = [
      '#type' => 'submit',
      '#value' => t('Download example file'),
      '#limit_validation_errors' => [],
      '#submit' => array('::downloadFile'),
    ];

    $vid = 'branches';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    $branches = [];
    foreach ($terms as $term) {
      $branches[$term->tid] = $term->name;
    }

    $form['branch'] = [
      '#type' => 'select',
      '#title' => $this->t('Branch to import students'),
      '#required' => TRUE,
      '#options' => $branches,
      '#default_value' => 'student',
    ];

    $form['select_csv_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Select CSV file'),
      '#upload_validators' => array(
        'file_validate_extensions' => array('csv'),
        'file_validate_size' => array(file_upload_max_size()),
      ),
      '#required' => TRUE,
    ];

    $form['message'] = array(
      '#prefix' => '<h5>' . t('Click "Show CVS" and check your data before run import.'),
      '#suffix' => '<h5>',
      '#weight' => 50,
    );

    $form['csv'] = array(
      '#prefix' => '<div id="test-decision-message">',
      '#suffix' => '</div>',
      '#weight' => 101,
    );

    $form['show_csv'] = [
      '#type' => 'button',
      '#value' => $this->t('Show CSV'),
      '#ajax' => [
        'callback' => [$this, 'callbackToTestDecision'],
        'wrapper' => 'test-decision-message',
        'effect' => 'fade',
      ],
      '#weight' => 99,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
      '#weight' => 100,
    ];

    return $form;
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function downloadFile(array &$form, FormStateInterface $form_state) {
    $module_handler = \Drupal::service('module_handler');
    $path = $module_handler->getModule('sa_data_import')->getPath();
    $buildUrl = 'base:/' . $path . '/data/students_import_example.csv';
    $url = Url::fromUri($buildUrl);
    $form_state->setRedirectUrl($url);
  }

  /**
   * Custom ajax callback to test entity.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @return array
   */
  public function callbackToTestDecision(array $form, FormStateInterface $form_state) {

    $message['csv'] = [
      '#prefix' => '<div id="test-decision-message">',
      '#markup' => '',
      '#suffix' => '</div>',
    ];

    $fid = $form_state->getValue('select_csv_file');

    if (!empty($fid)) {

      $form_state->setRebuild(TRUE);

      $file = File::load($fid[0]);
      $uri = $file->getFileUri();


      if (($handle = fopen($uri, "r")) !== FALSE) {
        $message['csv']['#markup'] .= '<table>';
        $i = 0;
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
          $message['csv']['#markup'] .= '<tr>';
          if ($i < 1000) {
            $message['csv']['#markup'] .= "<td>" . $i . "</td>";
            $error = '';
            foreach ($data as $key => $cell) {
              switch ($key) {
                case self::first_name:
                  if (empty($cell)) {
                    $error .= $this->t('Name is empty.');
                  }
                  break;
                case self::date_of_birth:
                  if (!empty($cell) && !$this->validateDate($cell)) {
                    $error .= $this->t('Date is invalid.');
                  }
                  break;
                case self::email:
                  if (!empty($cell) && !\Drupal::service('email.validator')->isValid($cell)) {
                    $error .= $this->t('The email address is not valid.');
                  }
                  break;
                case self::parent_email:
                  if (!empty($cell) && !\Drupal::service('email.validator')->isValid($cell)) {
                    $error .= $this->t('The parent email address is not valid.');
                  }
                  break;
                case self::source:
                  if ($i > 0) {
                    $tid = $this->getTidByName($cell, 'client_source');
                    if (empty($tid) && !empty($cell)) {
                      $cell = $cell . " " . "<span class='error'>" . $this->t('NOT exist') . "</span>";
                    }
                  }
                  break;
                case self::gender:
                  if (($cell != 'male') && ($cell != 'female')) {
                    $error .= $this->t('Gender is not valid.');
                  }
                  break;
                case self::category;
                  if ($i > 0) {
                    $cats = explode('|', $cell);
                    if (!empty($cats)) {
                      foreach ($cats as $c_key => $cat) {
                        $tid = $this->getTidByName(trim($cat), 'class_category');
                        if (empty($tid)) {
                          $cell = $cell . " " . "<span class='error'>" . $cat . ' - ' . $this->t('NOT exist') . "</span>";
                        }
                      }
                    }
                  }
                  break;
                case self::phone:
                  if ($i > 0) {
                    if (!empty($cell)) {
                      $cell = preg_replace('/\D/', '', $cell);
                    }
                  }
                  break;
                case self::status:
                  if ($i > 0) {
                    $tid = $this->getTidByName($cell, 'student_status');
                    if (empty($tid) && !empty($cell)) {
                      $cell = $cell . " " . "<span class='error'>" . $this->t('NOT exist') . "</span>";
                    }
                  }
                  break;
              }
              $message['csv']['#markup'] .= "<td>" . $cell . "</td>";
            }
            // Check login and email.
            if ($i > 0) {
              $query = \Drupal::entityQuery('user');
              $query->condition('field_phone', preg_replace('/\D/', '', $data[self::phone]));
              $user_ids = $query->execute();
              $message['csv']['#markup'] .= "<td>";
              if (!empty(user_load_by_mail(trim($data[self::email])))) {
                $message['csv']['#markup'] .= "<span class='error'>" . $this->t('User exist (will not be imported).') . "</span>";
              } elseif (!empty($user_ids)) {
                $message['csv']['#markup'] .= "<span class='warning'>" . $this->t('User with phone exist (will be imported).') . "</span>";
              }
              if (!empty($error)) {
                $message['csv']['#markup'] .= " " . "<span class='error'>" . $error . "</span>";
              } elseif (!empty($data[self::disabled]) && $i > 0) {
                $message['csv']['#markup'] .= " " . "<span class='warning'>" . $this->t('Will be DISABLED.') . "</span>";
              }
              $message['csv']['#markup'] .= "</td>";
            } else {
              $message['csv']['#markup'] .= "<td>" . "<span class='error'>" . $this->t('Errors') . "</span> / <span class='warning'>" . "<span class='warning'>" . $this->t('Warnings') . "</td>";
            }
            $i++;
          }
          $message['csv']['#markup'] .= '</tr>';
        }
        $message['csv']['#markup'] .= '</table>';
        fclose($handle);
      }

    }

    return $message['csv'];

  }


  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $fid = $form_state->getValue('select_csv_file');
    $branch = $form_state->getValue('branch');
    if (!empty($fid)) {
      $form_state->setRebuild(TRUE);
      $file = File::load($fid[0]);
      $uri = $file->getFileUri();
      $i = 1;
      if (($handle = fopen($uri, "r")) !== FALSE) {
        $first_line = TRUE;
        $batch = [
          'title' => t('Creating students...'),
          'operations' => [
          ],
        ];
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
          if (!$first_line && (count($data) == 17)) {
            $batch['operations'][] = [
              [$this, 'processItem'],
              [$data, $branch],
            ];
            $i++;
          }
          $first_line = FALSE;
        }
        fclose($handle);
        batch_set($batch);
      }
      drupal_set_message(t('@count users processed.', ['@count' => $i - 1]));
    } else {
      drupal_set_message(t('Empty or broken CSV file'));
    }
  }

  /**
   * Validate date.
   * @param $date
   * @return bool
   */
  public function validateDate($date) {
    $time = new \DateTime();
    $d = $time->createFromFormat('d.m.Y', $date);
    return $d && $d->format('d.m.Y') === $date;
  }

  /**
   * Utility: find term by name and vid.
   * @param null $name
   *  Term name
   * @param null $vid
   *  Term vid
   * @return int
   *  Term id or NULL if none.
   */
  protected function getTidByName($name = NULL, $vid = NULL) {
    $properties = [];
    if (!empty($name)) {
      $properties['name'] = $name;
    } else {
      $properties['name'] = " ";
    }
    if (!empty($vid)) {
      $properties['vid'] = $vid;
    }
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties($properties);
    $term = reset($terms);

    return !empty($term) ? $term->id() : NULL;
  }

  /**
   * Batch function.
   *
   * @param $item
   * @param $branch
   * @param $context
   */
  public function processItem($item, $branch, &$context) {
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 1;
    }
    $this->processBatchItem($item, $branch, $context['sandbox']['progress']);
    $context['sandbox']['progress']++;
  }

  /**
   * Process one item.
   *
   * @param $data
   * @param $branch
   * @param $key
   */
  public function processBatchItem($data, $branch, $key) {
    $name = $data[self::first_name];
    $surname = $data[self::last_name];
    $patronim = $data[self::patronymic];
    $date = $data[self::date_of_birth];
    $email = $data[self::email];
    $phone = preg_replace('/\D/', '', $data[self::phone]);
    if (!empty($name) && (empty($email) || \Drupal::service('email.validator')->isValid($email))) {
      if (user_load_by_mail($email) == FALSE) {
        // Create user object.
        $user = User::create();
        // Mandatory settings.
        if (!empty($date) && $this->validateDate($date)) {
          $time = new \DateTime();
          $date_object = $time->createFromFormat('d.m.Y', $date);
          $user->set('field_date_of_birth', $date_object->format('Y-m-d'));
        }
        $user->set('field_branch', $branch);
        $user->set('field_first_name', $name);
        $user->set('field_last_name', $surname);
        $user->set('field_patronymic', $patronim);
        $name = generate_new_unique_user_name();
        $user->setPassword($name);
        $user->setUsername($name);
        $user->set('field_phone', $phone);
        $user->addRole('student');
        $user->set('field_card_number', $data[self::card]);
        $user->set('field_employer_comment', $data[self::comment]);
        $user->set('field_parent_email', $data[self::parent_email]);
        $user->set('field_parent_name', $data[self::parent_name]);
        $user->set('field_parent_phone', $data[self::parent_phone]);
        // Terms.
        $source = trim($data[self::source]);
        $tid = $this->getTidByName($source, 'client_source');
        if ($tid) {
          $user->set('field_client_source', $tid);
        }
        $status = trim($data[self::status]);
        $tid = $this->getTidByName($status, 'student_status');
        if ($tid) {
          $user->set('field_student_stat', $tid);
        }
        // Categories.
        $cats = explode('|', $data[self::category]);
        $cats_to_add = [];
        if (!empty($cats)) {
          foreach ($cats as $c_key => $cat) {
            $tid = $this->getTidByName(trim($cat), 'class_category');
            if (!empty($tid)) {
              $cats_to_add[] = $tid;
            }
          }
        }
        $user->set('field_category', $cats_to_add);
        // Activate user if not disabled.
        if (empty($data[self::disabled])) {
          $user->set('status', 1);
        }
        // Set gender.
        $gender = $data[self::gender];
        if (!empty($gender) && (($gender == 'male') || ($gender == 'female'))) {
          $user->set('field_gender', $gender);
        }
        else {
          $user->set('field_gender', 'none');
        }
        // Child.
        if (!empty($data[self::child])) {
          $user->set('field_child', 1);
        }
        // Set imported.
        $user->set('field_imported', 1);
        // Set an email if email address exist.
        if (!empty($email)) {
          $user->setEmail($email);
        }
        // Save user.
        $user->save();
        if (!empty($email)) {
          //_user_mail_notify('register_admin_created', $user);
        }
      } else {
        drupal_set_message(t('User @name @surname with email "@email" already exist, line @i.', [
          '@name' => $name,
          '@surname' => $surname,
          '@email' => $email,
          '@i' => $key,
        ]), 'error');
      }
    } else {
      drupal_set_message(t('String @i is skipped, check line data.', ['@i' => $key]), 'warning');
    }
  }

}
