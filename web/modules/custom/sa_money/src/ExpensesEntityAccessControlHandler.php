<?php

namespace Drupal\sa_money;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Expenses entity entity.
 *
 * @see \Drupal\sa_money\Entity\ExpensesEntity.
 */
class ExpensesEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\sa_money\Entity\ExpensesEntityInterface $entity */
    switch ($operation) {

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit expenses entity entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete expenses entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add expenses entity entities');
  }

}
