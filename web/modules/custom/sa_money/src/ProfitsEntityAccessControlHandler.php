<?php

namespace Drupal\sa_money;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Profits entity entity.
 *
 * @see \Drupal\sa_money\Entity\ProfitsEntity.
 */
class ProfitsEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\sa_money\Entity\MoneyEntityInterface $entity */

    switch ($operation) {
      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit profits entity entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete profits entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::forbidden();
  }



  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add profits entity entities');
  }

}
