<?php

namespace Drupal\sa_money;

use Drupal\Core\Entity\EntityListBuilder;
use Drupal\views\Views;

/**
 * Defines a class to build a listing of Expenses entity entities.
 *
 * @ingroup sa_money
 */
class ExpensesEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   *
   * Builds the entity listing as renderable array for table.html.twig.
   */
  public function render() {
    $view = Views::getView('expenses');
    if (is_object($view)) {
      $view->setDisplay('block_list_with_filters');
      $view->preExecute();
      $view->execute();
      $build = $view->render();
    }
    $build['#attached']['library'][] =  'sa_admin_theme/fancy_select';
    return $build;
  }
}
