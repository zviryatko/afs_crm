<?php

/**
 * @file
 * Definition of Drupal\d8views\Plugin\views\field\PaymentBalance
 */

namespace Drupal\sa_money\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\NodeType;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\taxonomy\Entity\Term;

/**
 * Field handler to flag the node type.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("payment_balance")
 */
class PaymentBalance extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {

    $from = $values->_entity->get('from_timestamp')->value;
    $to = $values->_entity->get('to_timestamp')->value;
    $return = '<div class="name">' . $values->_entity->getName(). '</div>';

    if (!empty($from) && !empty($to)) {
      $abonnement = $values->_entity->get('field_abonnement')->target_id;
      $now = new \DateTime();
      $now_t = $now->getTimestamp();
      $to_object = new \DateTime(date('Y-m-d', $to));
      $from_object = new \DateTime(date('Y-m-d', $from));
      // Abonnement added but user limit to some time.
      if (!empty($abonnement)) {
        $term = Term::load($abonnement);
        $return = '<div class="name">' . $term->getName(). '</div>';
        $return .= '<div class="description">' . $term->get('description')->value . '</div>';
      }
      $from_text = date('Y-m-d', $from);
      $to_text = date('Y-m-d', $to);
      if ($now_t < $from) {
        $time_diff = $now->diff($from_object);
        $return .= '<div class="lefts">' . $this->t('@days days to start.', ['@days' => $time_diff->days]) . '</div>';
      }
      elseif ($now_t > $to) {
        $time_diff = $now->diff($to_object);
        $return .= '<div class="lefts">' . $this->t('Ended @days ago.', ['@days' => $time_diff->days]) . '</div>';
      }
      else {
        $time_diff = $now->diff($to_object);
        $return .= '<div class="lefts">' . $this->t('@days days left.', ['@days' => $time_diff->days]) . '</div>';
      }
      $return .= '<div class="from-to">' . $this->t('Valid from @from to @to', ['@from' => $from_text, '@to' => $to_text]) . '</div>';
    }

    return ['#markup' => $return];
  }
}