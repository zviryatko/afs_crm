<?php

namespace Drupal\sa_money\Plugin\views\area;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\DefaultSummary;

use Drupal\views\Plugin\views\area\AreaPluginBase;
/**
 * Views area handler to display total amount is exists.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("amount")
 */
class Amount extends AreaPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['content'] = [
      'default' => $this->t('Total amount @total'),
    ];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['content'] = [
      '#title' => $this->t('Display'),
      '#type' => 'textarea',
      '#rows' => 3,
      '#default_value' => $this->options['content'],
      '#description' => $this->t('Total amount dor displayed items, @total - token to replace.'),
    ];
  }


  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    $result = $this->view->result;
    $amount = 0;
    $format = $this->options['content'];
    foreach ($result as $row) {
      $amount += (float) $row->_entity->field_amount->value;
    }
    $replacements['@total'] = $amount;
    if ($amount <= 0) {
      $amount = t("Can't make a sum.");
    } else {
      $amount = Xss::filterAdmin(str_replace(array_keys($replacements), array_values($replacements), $format));
    }
    // Return as render array.
    return [
      '#markup' => "<h3>" . $amount . "</h3>",
    ];
  }
}
