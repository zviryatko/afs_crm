<?php

namespace Drupal\sa_money\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for deleting Expenses entity entities.
 *
 * @ingroup sa_money
 */
class MoneyEntityDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->getEntity();

    // Make sure that deleting a translation does not delete the whole entity.
    if (!$entity->isDefaultTranslation()) {
      $untranslated_entity = $entity->getUntranslated();
      $untranslated_entity->removeTranslation($entity->language()->getId());
      $untranslated_entity->save();
      $form_state->setRedirectUrl($untranslated_entity->urlInfo('canonical'));
    }
    else {
      $id = $entity->id();
      $type = $entity->getEntityType()->id();
      $entity->delete();
      // Delete visits.
      if ($type == 'profits_entity') {
        $query = \Drupal::entityTypeManager()->getStorage('visits_entity')->getQuery();
        $query->condition('payment', $id, '=');
        $results = $query->execute();
        if (!empty($results)) {
          foreach ($results as $result) {
            \Drupal::entityTypeManager()
              ->getStorage('visits_entity')
              ->load($result)
              ->delete();
          }
        }
      }

      $form_state->setRedirectUrl($this->getRedirectUrl());
    }

    drupal_set_message($this->getDeletionMessage());
    $this->logDeletionMessage();
  }
}
