<?php

namespace Drupal\sa_money\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Expenses entity edit forms.
 *
 * @ingroup sa_money
 */
class ExpensesEntityForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\sa_money\Entity\ExpensesEntity */
    $form = parent::buildForm($form, $form_state);

    // Add fancy select.
    $form['#attached']['library'][] =  'sa_admin_theme/fancy_select';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Expenses entity.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Expenses entity.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.expenses_entity.collection');
  }

}
