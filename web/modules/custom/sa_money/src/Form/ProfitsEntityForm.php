<?php

namespace Drupal\sa_money\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Form controller for Profits entity edit forms.
 *
 * @ingroup sa_money
 */
class ProfitsEntityForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\sa_money\Entity\ProfitsEntity */
    $form = parent::buildForm($form, $form_state);

    // Add fancy select.
    $form['#attached']['library'][] = 'sa_admin_theme/fancy_select';

    // Unset default value.
    $values = $form_state->getBuildInfo();
    if (!empty($values['form_id']) && $values['form_id'] == 'profits_entity_add_form') {
      unset($form['from_timestamp']['widget'][0]['value']['#default_value']);
      unset($form['to_timestamp']['widget'][0]['value']['#default_value']);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;

    // Apply coupon.
    $amount_before_coupon = $form_state->getValue('field_amount_before_coupon');
    $coupon = $form_state->getValue('field_coupon');

    if (!empty($coupon[0]['target_id']) && !empty($amount_before_coupon[0]['value'])) {
      $coupon_loaded = Term::load($coupon[0]['target_id']);
      if ($coupon_loaded) {
        $type = $coupon_loaded->get('field_coupon_type')->value;
        $number = $coupon_loaded->get('field_number')->value;
        if ($type == 'percent') {
          $result = round((float)$amount_before_coupon[0]['value'] * ((100 - (int)$number) / 100), 2);
        } elseif ($type == 'amount') {
          $result = (float)$amount_before_coupon[0]['value'] - (int)$number;
        }
        $entity->set('field_amount', $result);
      }
    } else {
      $entity->set('field_amount', $amount_before_coupon[0]['value']);

    }

    // Save.
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Profits entity.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Profits entity.', [
          '%label' => $entity->label(),
        ]));
    }

    // Create visits.
    // Check if abonnement selected.
    $abonnement = $form_state->getValue('field_abonnement');
    if (!empty($abonnement[0]['target_id'])) {

      $term = Term::load($abonnement[0]['target_id']);
      $components = $term->get('field_components');
      foreach ($components as $component) {
        $id = $component->target_id;
        $custom_block = \Drupal::entityTypeManager()->getStorage('block_content')->load($id);

        $lessons_count = $custom_block->get('field_lessons_count')->value;
        // If lesson count > 0 create visits
        if ($lessons_count > 0) {
          $i = 1;
          while ($i <= $lessons_count) {
            $query = \Drupal::entityTypeManager()->getStorage('visits_entity')->getQuery();
            $query->condition('name', $i, '=');
            $query->condition('payment', $entity->id(), '=');
            $query->condition('component', $id, '=');
            $result = $query->execute();
            if (empty($result)) {
              $data = array(
                'name' => $i,
                'payment' => $entity->id(),
                'component' => $id,
              );
              $visit = \Drupal::entityTypeManager()
                ->getStorage('visits_entity')
                ->create($data);
              $visit->save();
            }
            $i++;
          }
        }
        // Lesson count 0, unlimited. Create only one.
        $query = \Drupal::entityTypeManager()->getStorage('visits_entity')->getQuery();
        $query->condition('name', 1, '=');
        $query->condition('payment', $entity->id(), '=');
        $query->condition('component', $id, '=');
        $result = $query->execute();
        if (empty($result)) {
          $data = array(
            'name' => 1,
            'payment' => $entity->id(),
            'component' => $id,
          );
          $visit = \Drupal::entityTypeManager()
            ->getStorage('visits_entity')
            ->create($data);
          $visit->save();
        }
      }
    }
    $form_state->setRedirect('entity.profits_entity.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Get values.
    $abonnement = $form_state->getValue('field_abonnement');
    $user = $form_state->getValue('field_user');
    $amount = $form_state->getValue('field_amount_before_coupon');
    $class = $form_state->getValue('field_branch');

    // Check user and branch
    if (!empty($abonnement[0]['target_id']) &&
      ((empty($user) || empty($class)) || (empty($user) && empty($class)))) {
      $form_state->setError($form['field_user'], $this->t('Field "User" and "Branch" can not be empty if "Abonnement" selected.'));
    }
    $from = $form_state->getValue('from_timestamp');
    $to = $form_state->getValue('to_timestamp');

    // Check dates.
    if (!empty($abonnement[0]['target_id']) &&
      (empty($from[0]['value']) || empty($to[0]['value']))) {
      $form_state->setError($form['field_user'], $this->t('"Valid from" an "Valid to" can not be empty if "Abonnement" selected.'));
    }

    if (!empty($abonnement[0]['target_id'])) {
      $term = Term::load($abonnement[0]['target_id']);
      $price = $term->get('field_price')->value;
      if ($price != $amount[0]['value']) {
        $form_state->setError($form['field_amount_before_coupon'], $this->t('Check "Amount", @amount is not NOT equal to abonnement price @amount_abonnement.',
          [
            '@amount' => $amount[0]['value'],
            '@amount_abonnement' => $price,
          ]
        ));
      }
    }
  }
}
