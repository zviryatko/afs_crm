<?php
/**
 * @file
 * Contains \Drupal\sa_money\Form\CompareExpProfForm.
 */
namespace Drupal\sa_money\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class CompareExpProfForm extends FormBase {

  /**
   * Request stack.
   *
   * @var RequestStack
   */
  public $request;

  /**
   * Class constructor.
   *
   * @param RequestStack $request
   *   Request stack.
   */
  public function __construct(RequestStack $request) {
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('request_stack')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'resume_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $vid = 'branches';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    $term_data = ['_none' => t('All')];
    foreach ($terms as $term) {
      $term_data[$term->tid] = $term->name;
    }
    $date_time = new \DateTime();
    $month_ago = $date_time->setTimestamp(time() - (60 * 60 * 24 * 30));
    $date_time_now = new \DateTime();
    $today = $date_time_now->setTimestamp(time());

    $form['from'] = [
      '#title' => t('From'),
      '#type' => 'date',
      '#default_value' => $this->request->getCurrentRequest()->get('from') ? $this->request->getCurrentRequest()->get('from') : $month_ago->format('Y-m-d'),
      '#attributes' => [
        'type' => 'date',
        'placeholder' => 'yy-mm-dd',
      ],
      '#weight' => 1
    ];

    $form['to'] = [
      '#title' => t('To'),
      '#type' => 'date',
      '#default_value' => $this->request->getCurrentRequest()->get('to') ? $this->request->getCurrentRequest()->get('to') : $today->format('Y-m-d'),
      '#attributes' => [
        'type' => 'date',
        'placeholder' => 'yy-mm-dd',
      ],
      '#weight' => 2
    ];

    $form['branch'] = [
      '#title' => t('Branch'),
      '#type' => 'select',
      '#options' => $term_data,
      '#default_value' => $this->request->getCurrentRequest()->get('branch') ? $this->request->getCurrentRequest()->get('branch') : '_none',
      '#weight' => 3
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $from = $form_state->getValue('from');
    $to = $form_state->getValue('to');
    $branch = $form_state->getValue('branch');
    $url = Url::fromRoute('sa_money.compare_money', [], [
      'query' => [
        'from' => $from,
        'to' => $to,
        'branch' => $branch,
      ]
    ]);
    $form_state->setRedirectUrl($url);
  }
}