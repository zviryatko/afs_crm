<?php

namespace Drupal\sa_money\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Profits entity entities.
 *
 * @ingroup sa_money
 */
interface MoneyEntityInterface extends  ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Profits entity name.
   *
   * @return string
   *   Name of the Profits entity.
   */
  public function getName();

  /**
   * Sets the Profits entity name.
   *
   * @param string $name
   *   The Profits entity name.
   *
   * @return \Drupal\sa_money\Entity\ProfitsEntityInterface
   *   The called Profits entity entity.
   */
  public function setName($name);

  /**
   * Gets the Profits entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Profits entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Profits entity creation timestamp.
   *
   * @param int $timestamp
   *   The Profits entity creation timestamp.
   *
   * @return \Drupal\sa_money\Entity\ProfitsEntityInterface
   *   The called Profits entity entity.
   */
  public function setCreatedTime($timestamp);

}
