<?php

namespace Drupal\sa_money\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Expenses entity entities.
 */
class MoneyEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['expenses_entity']['status']['filter']['id'] = 'payment_status';

    return $data;
  }

}
