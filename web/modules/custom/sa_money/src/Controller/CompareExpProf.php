<?php
namespace Drupal\sa_money\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Component\Render\FormattableMarkup;

/**
 * Provides route responses for the Example module.
 */
class CompareExpProf extends ControllerBase {

  /**
   * Request stack.
   *
   * @var RequestStack
   */
  public $request;

  public $from_timestamp;
  public $to_timestamp;
  public $branch;

  /**
   * Class constructor.
   *
   * @param RequestStack $request
   *   Request stack.
   */
  public function __construct(RequestStack $request) {
    $this->request = $request;
    $from = $this->request->getCurrentRequest()->get('from');
    $to = $this->request->getCurrentRequest()->get('to');
    $this->branch = $this->request->getCurrentRequest()->get('branch');
    $date_time = new \DateTime();
    $month_ago = $date_time->setTimestamp(time() - (60 * 60 * 24 * 30));
    $date_time_now = new \DateTime();
    $today = $date_time_now->setTimestamp(time());
    $this->from_timestamp = $from ? \DateTime::createFromFormat('Y-m-d', $from)->format('U') : $month_ago->format('U');
    $this->to_timestamp = $to ? \DateTime::createFromFormat('Y-m-d', $to)->format('U') : $today->format('U');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('request_stack')
    );
  }

  private function getTableExp() {
    $query = \Drupal::database()->select('expenses_entity', 'ee');
    $query->fields('ee', ['id', 'name', 'date']);
    $query->fields('ee_s', ['field_amount_value']);
    $query->condition('status', 'payed', '=');
    $query->condition('date', $this->from_timestamp, '>');
    $query->condition('date', $this->to_timestamp, '<');
    $query->leftJoin('expenses_entity__field_branch', 'ee_b', 'ee.id=ee_b.entity_id');
    $query->leftJoin('expenses_entity__field_amount', 'ee_s', 'ee.id=ee_s.entity_id');
    if (!empty($branch) && $branch != '_none') {
      $query->condition('ee_b.field_branch_target_id', $this->branch, '=');
    }
    $result = $query->execute();
    $expenses = $result->fetchAll();
    $rows = [];
    foreach ($expenses as $expense) {
      $rows[] = [
        $expense->name,
        $expense->field_amount_value,
        \DateTime::createFromFormat('U', $expense->date)->format('d-m-Y'),
        [
          'data' => new FormattableMarkup('<a href=":link" target="_blank">@name</a>', [
            ':link' => '/admin/structure/expenses_entity/' . $expense->id . '/edit',
            '@name' => t('View'),
          ])
        ],
      ];
    }

    $header = [
      t('Expenses title (Payed)'),
      t('Sum'),
      t('Date'),
      '',
    ];

    return [
      '#type' => 'table',
      '#prefix' => t('<h3>Total: @sum</h3>', ['@sum' => $this->getTotal($rows)]),
      '#header' => $header,
      '#rows' => $rows,
    ];
  }

  private function getTableProf() {
    $query = \Drupal::database()->select('profits_entity', 'ee');
    $query->fields('ee', ['id', 'name', 'date']);
    $query->fields('ee_s', ['field_amount_value']);
    $query->condition('date', $this->from_timestamp, '>');
    $query->condition('date', $this->to_timestamp, '<');
    $query->leftJoin('profits_entity__field_branch', 'ee_b', 'ee.id=ee_b.entity_id');
    $query->leftJoin('profits_entity__field_amount', 'ee_s', 'ee.id=ee_s.entity_id');
    if (!empty($branch) && $branch != '_none') {
      $query->condition('ee_b.field_branch_target_id', $this->branch, '=');
    }
    $result = $query->execute();
    $expenses = $result->fetchAll();
    $rows = [];
    foreach ($expenses as $expense) {
      $rows[] = [
        $expense->name,
        $expense->field_amount_value,
        \DateTime::createFromFormat('U', $expense->date)->format('d-m-Y'),
        [
          'data' => new FormattableMarkup('<a href=":link" target="_blank">@name</a>', [
            ':link' => '/admin/structure/profits_entity/' . $expense->id . '/edit',
            '@name' => t('View'),
          ])
        ],
      ];
    }

    $header = [
      t('Profits title'),
      t('Sum'),
      t('Date'),
      '',
    ];

    return [
      '#type' => 'table',
      '#prefix' => t('<h3>Total: @sum</h3>', ['@sum' => $this->getTotal($rows)]),
      '#header' => $header,
      '#rows' => $rows,
    ];
  }


  /**
   * @return array
   */
  public function content() {
    $form = \Drupal::formBuilder()->getForm('Drupal\sa_money\Form\CompareExpProfForm');
    return [
      '#theme' => 'money_exp_prof',
      '#form' => $form,
      '#table_exp' => $this->getTableExp(),
      '#table_prof' => $this->getTableProf(),
    ];
  }

  /**
   * @param $rows
   * @return int
   */
  private function getTotal($rows) {
    $total = 0;
    foreach ($rows as $row) {
      $total += $row[1];
    }
    return $total;
  }

}
