/**
 * @file
 * Js file for add/edit class.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.saClass = {
    attach: function (context) {
      // Add fancy selects.
      $("select").select2();
      $(".select2-container").css("width", "200px");

      // Add time picker.
      $(".field--name-field-time-to input").each(function() {
        $(this).wickedpicker({
          twentyFour: true,
          title: '',
          minutesInterval: 5,
          now: $(this).val()
        });
      });

      $(".field--name-field-time-from input").each(function() {
        $(this).wickedpicker({
          twentyFour: true,
          title: '',
          minutesInterval: 5,
          now: $(this).val()
        });
      });

      // Fix css.
      $('.page-node-type-classes .page-content .region-content').css({'display': 'block'});
      $('.page-node-type-classes .page-content').css({'display': 'block'});

    }
  };

})(jQuery, Drupal, drupalSettings);