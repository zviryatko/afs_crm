/**
 * @file
 * Js file for add/edit class.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.fancySelectInit = {
    attach: function (context) {
      // Add fancy selects.
      $("select").select2();
      $(".select2-container").css("width", "300px");

      // Update fancy select width.
      $(".form-item-status .select2-container").css("width", "100px");
      $(".form-item-done .select2-container").css("width", "100px");
      $(".form-item-sort-bef-combine .select2-container").css("width", "100px");


    }
  };

})(jQuery, Drupal, drupalSettings);