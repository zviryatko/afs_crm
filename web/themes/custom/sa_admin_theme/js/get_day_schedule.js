/**
 * @file
 * Js file for get schedule.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.getDaySchedule = {
    attach: function (context) {

      var formDate = $("input.form-date", context);
      var path = drupalSettings.path.currentPath;
      var path_params = path.split('/');
      var timeFrom = $(".field--name-field-time-from input");
      var timeTo = $(".field--name-field-time-to input");
      var formClass = $(".form-item-field-class select");

      var getDaySchedule = function () {
        var date = formDate.val();
        var class_id = formDate.closest("form").find('.form-item-field-class select').val();
        var split = date.split('-');
        var url = drupalSettings.path.baseUrl + 'room-day-schedule/' + class_id + '/' + split[0] + '/' + split[1] + '/' + split[2];
        $.ajax({
          url: url,
          method: "GET",
          headers: {
            "Content-Type": "application/hal+json"
          },
          success: function (data, status) {
            if (status == 'success' && data.length > 0) {
              var html = '';
              $.each(data, function (index, value) {
                var current = '';
                if (path_params[1] == value.lesson_id) {
                  var current = ' current';
                }
                html += '<div class="' + value.class + current + '"><div class="time">' + value.label + '</div></div>';
              });

              $('#class-time-column').html(html).scrollTop(550);
            }
          }
        });
      };


      var disableDateTime = function () {
        formDate.prop("disabled", true);
        formDate.addClass('new-form-date-class');
        timeFrom.prop("disabled", true);
        timeTo.prop("disabled", true);
      };

      var enableDateTime = function () {
        $("input.new-form-date-class").prop("disabled", false);
        timeFrom.prop("disabled", false);
        timeTo.prop("disabled", false);
      };

      var body = $("body");
      if (body.hasClass('page-node-type-lesson')) {
        if (path_params[1].length > 0) {
          getDaySchedule();
          formDate.unbind();
          formDate.on('change', function () {
            getDaySchedule();
          });
        }
      } else if (body.hasClass('lesson-add-page')) {
        if (formClass.val() == '_none') {
          disableDateTime();
        } else {
          getDaySchedule();
        }
        formClass.unbind();
        formClass.on('change', function () {
          if ($(this).val() != '_none') {
            enableDateTime();
          }
        });
        formDate.unbind();
        formDate.on('click change', function () {
          getDaySchedule();
        });
      }

      // Add time picker.
      timeFrom.each(function () {
        $(this).wickedpicker({
          twentyFour: true,
          title: '',
          minutesInterval: 5,
          now: $(this).val()
        });
      });

      timeTo.each(function () {
        $(this).wickedpicker({
          twentyFour: true,
          title: '',
          minutesInterval: 5,
          now: $(this).val()
        });
      });
    }
  };

})(jQuery, Drupal, drupalSettings);
