/**
 * @file
 * Js file for add/edit class.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.abonnement = {
    attach: function (context) {

      var classes = $('.field--name-field-classes');
      var categories = $('.field--name-field-categories');

      $('.field--name-field-lesson-limit input').on('change', function() {
        var limit = $(this).val();
        switch (limit) {
          case 'no_limit':
            classes.hide();
            categories.hide();
            break;
          case 'by_class':
            classes.show();
            categories.hide();
            break;
          case 'by_category':
            categories.show();
            classes.hide();
            break;
        }
      });
      var limit = $('.field--name-field-lesson-limit input:checked').val()
      switch (limit) {
        case 'no_limit':
          classes.hide();
          categories.hide();
          break;
        case 'by_class':
          classes.show();
          categories.hide();
          break;
        case 'by_category':
          categories.show();
          classes.hide();
          break;
      }

    }
  };

})(jQuery, Drupal, drupalSettings);