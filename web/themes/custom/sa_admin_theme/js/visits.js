/**
 * @file
 * Js file for add/edit class.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.AjaxCommands.prototype.reloadPage = function(){
    location.reload();
  };

  Drupal.behaviors.visit = {
    attach: function () {

      var form = $('.visit-form-wrapper form');

      form.once().on('change', function (element) {
        var name = element.target.getAttribute('name');
        var date = $(this).find('.form-item-date input').val();
        var lesson = $(this).find('.form-item-lesson select').val();
        var status = $(this).find('.form-item-status select').val();
        var oldUrl = $(this).attr("data-href");
        var newUrl = oldUrl + '/' + name + '/' + date + '/' + lesson + '/' + status;
        var drupalObject = Drupal.ajax({
          url: newUrl,
          method: "POST",
          progress: {type: 'fullscreen'}
        });
        drupalObject.execute();
      });
    }
  };

})(jQuery, Drupal, drupalSettings);
