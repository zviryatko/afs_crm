/**
 * @file
 * Js file for calendar.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.saCalendar = {
    attach: function () {

      // Calendar.
      var url = drupalSettings.path.baseUrl + 'admin/rest/lessons';
      if (typeof drupalSettings.path.currentQuery !== "undefined") {
        url = url + '?' + $.param(drupalSettings.path.currentQuery);
      }
      $.ajax({
        url: url,
        method: "GET",
        headers: {
          "Content-Type": "application/hal+json"
        },
        success: function (data, status) {

          if (status == 'success' && data.length > 0) {

            $.each(data, function (index, value) {
              data[index].start = value.field_date + 'T' + value.time_from;
              data[index].end = value.field_date + 'T' + value.time_to;
              data[index].title = value.class + ': ' + value.title;
            });

            $('#calendar').fullCalendar({
              customButtons: {
                myCustomButton: {
                  theme: 'true',
                  text: Drupal.t('set date'),
                  click: function () {
                    $('#datepicker').toggle();
                  }
                }
              },
              defaultDate: moment(),
              header: {
                left: 'myCustomButton prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
              },

              defaultView: 'agendaWeek',
              events: function (start, end, timezone, callback) {
                callback(data);
              },

              eventClick: function (calEvent, jsEvent, view) {
                Drupal.ajax({
                  url: drupalSettings.path.baseUrl + 'lessons-for-calendar/' + calEvent.nid,
                  success: function(response) {
                    var viewLink = '<a  class="button lesson-edit" target="_blank" href="' + drupalSettings.path.baseUrl + 'node/' + calEvent.nid + '">' + Drupal.t('View') + '</a>';
                    var editLink = '<a  class="button lesson-edit" target="_blank" href="' + drupalSettings.path.baseUrl + 'node/' + calEvent.nid + '/edit' + '">' + Drupal.t('Edit') + '</a>';
                    var deleteLink = '<a class="button lesson-edit" target="_blank" href="' + drupalSettings.path.baseUrl + 'node/' + calEvent.nid + '/delete' + '">' + Drupal.t('Delete') + '</a>';
                    var $myDialog = $('<div class="lesson-modal">' + response[2].data + viewLink + editLink + deleteLink +'</div>');
                    Drupal.dialog($myDialog, {title: calEvent.title, width: 'auto'}).showModal();
                  }
                }).execute();

              }

            });
          }
        }
      });

      // Datepicker.
      $('#datepicker').datepicker({
        inline: true,
        onSelect: function(dateText, inst) {
            var d = new Date(dateText);
            $('#calendar').fullCalendar('gotoDate', d);
        }
      });

    }
  };

})(jQuery, Drupal, drupalSettings);