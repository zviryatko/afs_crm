/**
 * @file
 * Js file for add/edit class.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.lessonsReplace = {
    attach: function () {

      // Lessons select.
      var lessons_actions = function(class_object) {
        var lesson_select = $('form .form-item-lesson select');
        var value = class_object.val();
        if ((value.length > 0) && (value != '_none')) {
          lesson_select.find('option[value=_none]').prop('selected', true);
          var url = drupalSettings.path.baseUrl + 'lessons-by-class/' + value;
          $.ajax({
            url: url,
            method: "GET",
            headers: {
              "Content-Type": "application/hal+json"
            },
            success: function (data, status) {
              if (status == 'success' && data.length > 0) {
                lesson_select.find('option').each(function(){
                  var value = $(this).val();
                  if(($.inArray(value, data) == -1) || value == '_none'){
                    $(this).prop('disabled', true);
                  }else{
                    $(this).prop('disabled', false);
                  }
                });
              }
            }
          });
          lesson_select.prop('disabled', false);
          lesson_select.select2();
          $(".select2-container").css("width", "300px");
        }
        else {
          lesson_select.find('option[value=_none]').prop('selected', true);
          lesson_select.prop('disabled', true);
          lesson_select.trigger('change');
        }
      };

      var item = $('form .form-item-class select');
      var lesson_select = $('form .form-item-lesson select');
      var selected_lesson = lesson_select.val();
      lessons_actions(item);
      lesson_select.find('option[value=' + selected_lesson + ']').prop('selected', true);
      lesson_select.trigger('change');

      item.once().on('change', function (element) {
        lessons_actions($(this));
      });

    }
  };

})(jQuery, Drupal, drupalSettings);
